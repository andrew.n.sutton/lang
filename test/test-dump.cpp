
#include "test.hpp"

int 
main()
{
  expr* u = make_unit();
  expr* t = make_true();
  expr* f = make_false();
  expr* p = make_ph();

  dump(u);
  dump(t);
  dump(f);
  dump(p);

  expr* e1 = new seq_expr(new seq_expr(t, f), u);
  dump(e1);
}
