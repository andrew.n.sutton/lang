
#pragma once

#include "ast.hpp"

// -------------------------------------------------------------------------- //
// Type builders

/// A helper function for defining singleton types.
template<typename T>
inline type*
base_type()
{
  static T t;
  return &t;
}

/// Returns the type `Unit`.
type*
get_unit_type()
{
  return base_type<unit_type>();
}

/// Returns the type `Bool`.
type*
get_bool_type()
{
  return base_type<bool_type>();
}

/// Returns the type type `Int`.
type*
get_int_type()
{
  return base_type<int_type>();
}

/// Returns the type of placeholders.
type*
get_ph_type()
{
  return base_type<ph_type>();
}

type*
get_arrow_type(type* t1, type* t2)
{
  return new arrow_type(t1, t2);
}

// -------------------------------------------------------------------------- //
// Expression builders

/// Returns the unit literal.
inline expr*
make_unit()
{
  static unit_expr u;
  return &u;
}

/// Returns the literal true.
inline expr* 
make_true()
{
  static bool_expr t = true;
  return &t;
}

/// Returns the literal false.
inline expr* 
make_false()
{
  static bool_expr f = false;
  return &f;
}

/// Returns a new boolean literal.
inline expr* 
make_bool(bool b)
{
  return b ? make_true() : make_false();
}

/// Returns a placeholder expression.
inline expr* 
make_ph()
{
  static ph_expr e;
  return &e;
}

/// Returns a reference to a variable.
inline expr* 
make_ref(var* v)
{
  return new ref_expr(v);
}

