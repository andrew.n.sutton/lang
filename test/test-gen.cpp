
#include "test.hpp"

#include <cassert>


/// Returns the abstraction \x.x.
expr* 
make_id_int(environment& env)
{
  scope_guard g(env);
  var* x = env.declare("x", get_int_type());
  expr* e = new fn_expr({x}, new ref_expr(x));
  typecheck(e);
  return e;
}

expr* 
make_add_int(environment& env)
{
  scope_guard g(env);
  var* x = env.declare("x", get_int_type());
  var* y = env.declare("y", get_int_type());
  expr* e = new fn_expr({x, y},
    new arith_expr(op_add, 
      new ref_expr(x), 
      new ref_expr(y)));
  typecheck(e);
  return e;
}

int 
main()
{
  environment env;

  expr* u = make_unit();

  expr* id_fn = make_id_int(env);
  expr* add_fn = make_add_int(env);

  // It's probably okay to add these names in the same context.
  scope_guard g1(env);
  var* id_name = env.declare("id", id_fn->ty);
  var* add_name = env.declare("add", add_fn->ty);

  expr* mod = 
    new let_expr(id_name, id_fn, 
    new let_expr(add_name, add_fn, u));


  // dump(mod);

  // compile(mod);
}
