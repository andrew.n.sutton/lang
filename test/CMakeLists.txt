
set(CMAKE_CXX_FLAGS "-std=c++14")

include_directories(${CMAKE_SOURCE_DIR})

add_library(lc-test
  test.cpp
)
target_link_libraries(lc-test lc-core)


macro(add_unit_test tgt)
  add_executable(${tgt} ${ARGN}) 
  target_link_libraries(${tgt} lc-test)
  add_test(${tgt} ${tgt})
endmacro()

add_unit_test(test-dump test-dump.cpp)
add_unit_test(test-check test-check.cpp)
add_unit_test(test-gen test-gen.cpp)
