
#include "test.hpp"

#include <cassert>

int 
main()
{
  type* U = get_unit_type();
  type* B = get_bool_type();
  type* Z = get_int_type();
  type* P = get_ph_type();

  expr* u = make_unit();
  expr* t = make_true();
  expr* f = make_false();
  expr* p = make_ph();

  assert(equal(typecheck(u), U));
  assert(equal(typecheck(t), B));
  assert(equal(typecheck(f), B));
  assert(equal(typecheck(p), P));
}
