#include "ast.hpp"

#include <iostream>

/// Provides context for dumping trees.
struct dumper
{
  dumper(std::ostream& os)
    : os(os), depth(0)
  { }

  void indent() { ++depth; }
  void undent() { --depth; }

  void tab()
  {
    os << std::string(2 * depth, ' ');
  }

  void newline()
  {
    os << '\n';
  }

  void entry(const char* name, const void* p)
  {
    tab();
    os << "\033[1;32m" << name << "\033[0m" << ' ' 
       << "\033[1;36m" << p << "\033[0m";
  }

  std::ostream& os;
  int depth;
};

/// A helper class for managing indentation.
struct nested_scope
{
  nested_scope(dumper& d)
    : d(d)
  {
    d.newline();
    d.indent();
  }

  ~nested_scope()
  {
    d.undent();
  }
  
  dumper& d;
};

// -------------------------------------------------------------------------- //
// Variables

void
dump_var(dumper& d, const var* v)
{
  d.entry("var", v);
  d.os << " name=" << v->name;
  d.newline();
}

void
dump(dumper& d, const var* v)
{
  return dump_var(d, v);
}

// -------------------------------------------------------------------------- //
// Expressions

/// Returns a textual spelling of the node's name.
const char*
node_name(const expr* e)
{
  switch (e->kind) {
#define def_expr(E) \
  case ek_ ##E: \
    return #E "_expr";
#include "expr.def"
  }
}

/// Prints the entry line for the node.
void
dump_entry(dumper& d, const expr* e)
{
  d.entry(node_name(e), e);
}

/// By default print nothing.
void
dump_expr_attrs(dumper& d, const expr* e)
{
  // Nothing to do.
}

void
dump_expr_attrs(dumper& d, const bool_expr* e)
{
  d.os << " value=" << (e->val ? "true" : "false");
}

void
dump_expr_attrs(dumper& d, const int_expr* e)
{
  d.os << " value=" << e->val;
}

void
dump_expr_attrs(dumper& d, const arith_expr* e)
{
  d.os << " operator=" << get_spelling(e->op);
}

void
dump_expr_attrs(dumper& d, const rel_expr* e)
{
  d.os << " operator=" << get_spelling(e->op);
}

void
dump_expr_attrs(dumper& d, const proj_expr* e)
{
  d.os << " elem=" << e->elem;
}

/// Optionally prints attributes for the expression. Provide an overload
/// of dump_expr_attrs to emit additional information.
void
dump_attrs(dumper& d, const expr* e)
{
  switch (e->kind) {
#define def_expr(E) \
    case ek_ ## E: \
      return dump_expr_attrs(d, (const E ## _expr*)e);
#include "expr.def"
  }
}

// By default, print a newline.
void 
dump_expr_children(dumper& d, const expr* e)
{
  d.newline();
}

void
dump_expr_children(dumper& d, const ref_expr* e)
{
  nested_scope g(d);
  dump(d, e->ref);
}

void
dump_expr_children(dumper& d, const abs_expr* e)
{
  nested_scope g(d);
  dump(d, e->parm);
  dump(d, e->body);
}

void
dump_expr_children(dumper& d, const app_expr* e)
{
  nested_scope g(d);
  dump(d, e->abs);
  dump(d, e->arg);
}

void
dump_expr_children(dumper& d, const cond_expr* e)
{
  nested_scope g(d);
  dump(d, e->cond);
  dump(d, e->tval);
  dump(d, e->fval);
}

void
dump_expr_children(dumper& d, const and_expr* e)
{
  nested_scope g(d);
  dump(d, e->lhs);
  dump(d, e->rhs);
}

void
dump_expr_children(dumper& d, const or_expr* e)
{
  nested_scope g(d);
  dump(d, e->lhs);
  dump(d, e->rhs);
}

void
dump_expr_children(dumper& d, const not_expr* e)
{
  nested_scope g(d);
  dump(d, e->arg);
}

void
dump_expr_children(dumper& d, const arith_expr* e)
{
  nested_scope g(d);
  dump(d, e->lhs);
  dump(d, e->rhs);
}

void
dump_expr_children(dumper& d, const rel_expr* e)
{
  nested_scope g(d);
  dump(d, e->lhs);
  dump(d, e->rhs);
}

void
dump_expr_children(dumper& d, const tuple_expr* e)
{
  nested_scope g(d);
  for (expr* x : e->elems)
    dump(d, x);
}

void
dump_expr_children(dumper& d, const proj_expr* e)
{
  nested_scope g(d);
  dump(e->base);
}

void
dump_expr_children(dumper& d, const fn_expr* e)
{
  nested_scope g(d);  
  for (var* v : e->parms)
    dump(d, v);
  dump(d, e->body);
}

void
dump_expr_children(dumper& d, const call_expr* e)
{
  nested_scope g(d);  
  dump(d, e->abs);
  for (expr* a : e->args)
    dump(d, a);
}

void
dump_expr_children(dumper& d, const bind_expr* e)
{
  nested_scope g(d);  
  dump(d, e->abs);
  for (expr* a : e->args)
    dump(d, a);
}

void
dump_expr_children(dumper& d, const let_expr* e)
{
  nested_scope g(d);  
  dump(d, e->parm);
  dump(d, e->init);
  dump(d, e->body);
}

void
dump_expr_children(dumper& d, const seq_expr* e)
{
  nested_scope g(d);  
  dump(d, e->first);
  dump(d, e->second);
}

/// Optionally dump children of the expression. By default, this simply
/// terminates the line. Provide an overload of dump_expr_children to emit the 
/// additional information.
///
/// \todo It would be great if we had an API that exposed a direct list of
/// children. 
void
dump_children(dumper& d, const expr* e)
{
  switch (e->kind) {
#define def_expr(E) \
    case ek_ ## E: \
      return dump_expr_children(d, (const E ## _expr*)e);
#include "expr.def"
  }
}

void
dump(dumper& d, const expr* e)
{
  dump_entry(d, e);
  dump_attrs(d, e);
  dump_children(d, e);
}

void
dump(const expr* e)
{
  dumper d(std::cerr);
  dump(d, e);
}
