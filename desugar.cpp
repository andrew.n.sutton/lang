
#include "ast.hpp"

/// FIXME: This isn't really useful unless we want to reduce towards pure
/// lambda calculus.

expr*
desugar_fn(fn_expr* e)
{
  // Build the fn abstraction from the inside out.
  auto iter = e->parms.rbegin();
  expr* abs = new abs_expr(*iter++, e->body);
  while (iter != e->parms.rend())
    abs = new abs_expr(*iter++, abs);
  return abs;
}

expr*
desugar_call(call_expr* e)
{
  expr* arg = e->abs;
  for (expr* a : e->args)
    arg = new app_expr(arg, a);
  return arg;
}

expr*
desugar_bind(bind_expr* e)
{
  return e;
}

expr*
desugar_let(let_expr* e)
{
  return new app_expr(new abs_expr(e->parm, e->body), e->init);
}

expr*
desugar_seq(seq_expr* e)
{
  environment env;
  scope_guard g(env);
  var* dummy = env.declare("_");
  return new app_expr(new abs_expr(dummy, e->second), e->first);
}

expr*
desugar(expr* e)
{
  switch (e->kind) {
    case ek_ref:
    case ek_abs:
    case ek_app:
    case ek_unit:
    case ek_bool:
    case ek_cond:
    case ek_ph:
      return e;
    case ek_fn:
      return desugar_fn(static_cast<fn_expr*>(e));
    case ek_call:
      return desugar_call(static_cast<call_expr*>(e));
    case ek_bind:
      return desugar_bind(static_cast<bind_expr*>(e));
    case ek_let:
      return desugar_let(static_cast<let_expr*>(e)); 
    case ek_seq:
      return desugar_seq(static_cast<seq_expr*>(e));
    default:
      throw std::logic_error("not implemented");
  }
}
