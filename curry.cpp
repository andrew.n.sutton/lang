
#include "ast.hpp"

#include <cassert>

expr*
curry(expr* e)
{
  assert(e->kind == ek_fn);
  return desugar(e);
}

expr*
uncurry(expr* e)
{
  var_seq parms;
  while (e->kind == ek_abs) {
    abs_expr* abs = static_cast<abs_expr*>(e);
    parms.push_back(abs->parm);
    e = abs->body;
  }
  return new fn_expr(std::move(parms), e);
}
