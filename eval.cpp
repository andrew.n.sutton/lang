
#include "ast.hpp"

#include <cassert>

// The evaluator maintains multiple stores with various capabilities. Formally,
// the program memory is maintained as m = (s, a, f) where s, a, and f are
// different kind so of object stores:
//
//    s -- the static store associates global variables with objects.
//    a -- the automatic store associates local variables with objects.
//    f -- the free store maintain a list of allocated objects.
//
// These stores are updated by variable declarations, abstractions, and by
// by dynamic allocations.
//
// NOTE: There is no syntax that declares static variables. That would be
// part of the declaration, possibly the type (if we had typed locations).
//
// The big-step evaluation relation is e | m => e' | m'.
//
// Evaluation requires a binding context S (Sigma) which maintains an 
// association of bindings between variables and their locations, denoted
// x @ l.

// -------------------------------------------------------------------------- //
// Initialization and assignment

void
initialize(location loc, const value& init, store& sto)
{
  value& obj = sto.resolve(loc);
  assert(obj.kind == vk_undef);
  obj = init;
}

/// FIXME: Verify that the original and new values have the same shape.
/// That is, we can't replace an object of one with a fundamentally different
/// structure. That would indicate a logical error in the eval rules.
void
assign(location loc, const value& val, store& sto)
{
  value& obj = sto.resolve(loc);
  assert(obj.kind == val.kind);
  obj = val;
}

// -------------------------------------------------------------------------- //
// Variable lookup


// FIXME: Search the static store if x has static storage.
value
evaluate(const var* x, store& sto)
{
  location loc = sto.stack.lookup(x);
  assert(loc);
  return value(loc);
}

// -------------------------------------------------------------------------- //
// Expressions

value evaluate(const expr* e, store& sto);

/// The value of a (variable) reference is the one to which it is bound.
///
///            x @ l in S
/// ----------------------------------- [Eval-Var]
/// S |- x | <s, a, f> => l | <s, a, f>
value
evaluate_ref_expr(const ref_expr* e, store& sto)
{
  return evaluate(e->ref, sto);
}

///            S |- e1 | <s, a, f> => \x:T.e | <s', a', f'>
///           S |- e2 | <s', a', f'> => v2 | <s'', a'', f''>
///                           l = alloc(a)
/// S, x @ l |- e | <s'', (a'',l = v2), f''> => v | <s''', a''', f'''>
/// -------------------------------------------------------------------------- [Eval-App]
///          S |- e1 e2 | <s, a, f> => v | <s''', f''', a'''>
///
/// This bears some explanation... Here, e1 evaluates to an abstraction value,
/// and e2 evaluates (possibly) independently to some other value. The 
/// evaluation of either may modify any store in the program's memory. After 
/// evaluating the argument, we set up the call by creating a new call frame. 
/// In order to call the abstraction, we need to simultaneously bind the 
/// parameter name to an location in that frame (l = alloc(a)), and 
/// initializing the corresponding object with the argument (l = v2).
value
evaluate_app_expr(const app_expr* e, store& sto)
{
  value abs = evaluate(e->abs, sto);
  value arg = evaluate(e->arg, sto);

  assert(abs.kind == vk_abs);
  const abs_expr* fn = (const abs_expr*)abs.rep.ex;

  // Set up the stack frame.
  frame_guard f(sto);
  location loc = sto.stack.allocate(fn->parm);
  initialize(loc, arg, sto);

  // Evaluate the body.
  return evaluate(fn->body, sto);
}

/// --------------------- [Eval-Abs]
/// S |- \x:T.e => \x:T.e
value
evaluate_abs_expr(const abs_expr* e, store& sto)
{
  return value(e);
}

/// ----------------- [Eval-Unit]
/// S |- unit => unit
value
evaluate_unit_expr(const unit_expr* e, store& sto)
{
  return value(unit_v);
}

value
evaluate_bool_expr(const bool_expr* e, store& sto)
{
  return value(e->val);
}

/// S |- e1 | m => true | m'   S |- e2 | m' => v | m''
/// -------------------------------------------------- [Eval-Cond-True]
///     S |- if e1 then e2 else e3 | m => v | m''
///
/// S |- e1 | m => false | m'   S |- e3 | m' => v | m''
/// --------------------------------------------------- [Eval-Cond-False]
/// S |- if e1 then e2 else e3 | m => v | m''
value
evaluate_cond_expr(const cond_expr* e, store& sto)
{
  value cond = evaluate(e->cond, sto);
  if (cond.kind != vk_int)
    throw std::runtime_error("invalid condition");
  if (cond.rep.num)
    return evaluate(e->tval, sto);
  else
    return evaluate(e->fval, sto);
}

///    S |- e1 | m => false | m'
/// ------------------------------- [Eval-And-False]
/// S |- e1 and e2 | m => false | m' 
///
/// S |- e1 | m => true | m'   S |- e2 | m' => v | m''
/// -------------------------------------------------- [Eval-And-True]
///         S |- e1 and e2 | m => v | m''
value
evaluate_and_expr(const and_expr* e, store& sto)
{
  value lhs = evaluate(e->lhs, sto);
  if (lhs.rep.num)
    return evaluate(e->rhs, sto);
  else
    return lhs;
}

///    S |- e1 => true
/// --------------------- [Eval-Or-True]
/// S |- e1 or e2 => true
///
/// S |- e1 => false   S |- e2 => v
/// ------------------------------- [Eval-Or-False]
///      S |- e1 or e2 => v
value
evaluate_or_expr(const or_expr* e, store& sto)
{
  value lhs = evaluate(e->lhs, sto);
  if (lhs.rep.num)
    return lhs;
  else
    return evaluate(e->rhs, sto);
}

/// S |- e1 => true
/// ---------------- [Eval-Not-True]
/// S |- e1 => false
///
/// S |- e1 => false
/// ---------------- [Eval-Not-False]
/// S |- e1 => true
value
evaluate_not_expr(const not_expr* e, store& sto)
{
  value arg = evaluate(e->arg, sto);
  return value(!arg.rep.num);
}

/// ----------- [Eval-Int]
/// S |- n => n
value
evaluate_int_expr(const int_expr* e, store& sto)
{
  return value(e->val);
}

/// S |- e1 => v1   S |- e2 => v2
/// ----------------------------- [Eval-Arith]
///    S |- e1 @ e2 => v1 @ v2
///
/// FIXME: Detect overflow!
///
/// FIXME: These operations depend on the types of the operands. We would
/// need to handle these differently for FP types.
value
evaluate_arith_expr(const arith_expr* e, store& sto)
{
  value v1 = evaluate(e->lhs, sto);
  value v2 = evaluate(e->rhs, sto);
  switch (e->op) {
  case op_add: return value(v1.rep.num + v2.rep.num);
  case op_sub: return value(v1.rep.num - v2.rep.num);
  case op_mul: return value(v1.rep.num * v2.rep.num);
  case op_div: return value(v1.rep.num / v2.rep.num);
  case op_rem: return value(v1.rep.num % v2.rep.num);

  case op_and: return value(v1.rep.num & v2.rep.num);
  case op_ior: return value(v1.rep.num | v2.rep.num);
  case op_xor: return value(v1.rep.num ^ v2.rep.num);
  case op_shl: return value(v1.rep.num << v2.rep.num);
  case op_shr: return value(v1.rep.num >> v2.rep.num);
  }
}

/// S |- e1 => v1   S |- e2 => v2
/// ----------------------------- [Eval-Rel]
///    S |- e1 @ e2 => v1 @ v2
///
/// \todo These operations depend on the types of the operands.
value
evaluate_rel_expr(const rel_expr* e, store& sto)
{
  value v1 = evaluate(e->lhs, sto);
  value v2 = evaluate(e->rhs, sto);
  switch (e->op) {
  case op_eq: return value(v1.rep.num == v2.rep.num);
  case op_ne: return value(v1.rep.num != v2.rep.num);
  case op_lt: return value(v1.rep.num < v2.rep.num);
  case op_gt: return value(v1.rep.num > v2.rep.num);
  case op_le: return value(v1.rep.num <= v2.rep.num);
  case op_ge: return value(v1.rep.num >= v2.rep.num);
  }
}

///    for each i in [0, n], S |- ei => vi
/// ------------------------------------------- [Eval-Tuple]
/// S |- {e1, e2, ..., en} => {v1, v2, ..., vn}
value
evaluate_tuple_expr(const tuple_expr* e, store& sto)
{
  agg_value elems(e->elems.size());
  std::transform(e->elems.begin(), e->elems.end(), elems.begin(), [&sto](const expr* x) {
    return evaluate(x, sto);
  });
  return value(std::move(elems));
}

/// S |- e => {v1, v2, ..., vk}
/// --------------------------- [Eval-Proj]
///       S |- e.n => vn
value
evaluate_proj_expr(const proj_expr* e, store& sto)
{
  value base = evaluate(e->base);
  agg_value& agg = base.rep.agg;
  return agg[e->elem];
}

value
evaluate_new_expr(const new_expr* e, store& sto)
{
  value init = evaluate(e->init, sto);
  location loc = sto.free.allocate();
  initialize(loc, init, sto);
  return value(loc);
}

value
evaluate_del_expr(const del_expr* e, store& sto)
{
  value obj = evaluate(e->obj, sto);
  sto.free.deallocate(obj.rep.loc);
  return value(unit_v);
}

value
evaluate_addr_expr(const addr_expr* e, store& sto)
{
  // Yes... this is correct.
  return evaluate(e->obj, sto);
}

value
evaluate_deref_expr(const deref_expr* e, store& sto)
{
  /// Yes... this is correct.
  return evaluate(e->ptr, sto);
}

value
evaluate_load_expr(const load_expr* e, store& sto)
{
  value obj = evaluate(e->obj, sto);
  return sto.resolve(obj.rep.loc);
}

value
evaluate_store_expr(const store_expr* e, store& sto)
{
  value obj = evaluate(e->obj, sto);
  value val = evaluate(e->val, sto);
  assign(obj.rep.loc, val, sto);
  return obj;
}


/// ----------------------------------------------------- [Eval-Lambda]
/// S |- \(x1:T1, x2:T2, ...).e => \(x1:T1, x2:T2, ...).e
value
evaluate_fn_expr(const fn_expr* e, store& sto)
{
  return value(e);
}

/// S |- e => \(x1:T1, x2:T2, ..., xn:Tn)
///  for each i in [0, n], S |- ei => vi
/// ------------------------------------- [Eval-Call]
/// S |- e(e1, e2, ..., en) => v
value
evaluate_call_expr(const call_expr* e, store& sto)
{
  value abs = evaluate(e->abs, sto);
  value_seq args;
  for (const expr* a : e->args)
    args.push_back(evaluate(a, sto));

  assert(abs.kind == vk_fn);
  const fn_expr* fn = (const fn_expr*)abs.rep.ex;
  assert(fn->parms.size() == args.size());

  /// Set up the call frame.
  frame_guard f(sto);
  auto vi = args.begin();
  auto pi = fn->parms.begin();
  while (vi != args.end()) {
    location loc = sto.stack.allocate(*pi++);
    initialize(loc, *vi++, sto);
  }

  return evaluate(fn->body, sto);
}

value
evaluate_ph_expr(const ph_expr* e, store& sto)
{
  throw std::logic_error("evaluation of placeholder");
}

value
evaluate_bind_expr(const bind_expr* e, store& sto)
{
  throw std::logic_error("evaluation of bind");
}

/// S |- e1 => v1   S, x = v1 |- e2 => v2
/// -------------------------------------
///      S |- let x = e1 in e2 => v2
value
evaluate_let_expr(const let_expr* e, store& sto)
{
  // FIXME: C++ language rules allow the initializer to refer to the declared, 
  // but initialized object. Support that? We'd just have to move this line
  // of code after the allocation.
  value init = evaluate(e->init);

  frame_guard f(sto);
  location loc = sto.stack.allocate(e->parm);
  initialize(loc, init, sto);
  
  return evaluate(e->body, sto);
}

/// S |- e1 => v   S |- e2 => v2
/// ---------------------------- [Eval-Seq]
///      S |- e1; e2 => v2
value
evaluate_seq_expr(const seq_expr* e, store& sto)
{
  // Evaluate and discard the first expression.
  (void)evaluate(e->first, sto);

  // Evaluate and return the second.
  return evaluate(e->second, sto);
}

value 
evaluate(const expr* e, store& sto)
{
  switch (e->kind) {
#define def_expr(E) \
  case ek_ ## E: \
    return evaluate_ ## E ## _expr((const E ##_expr*)e, sto);
#include "expr.def"
  }
}

value
evaluate(const expr* e)
{
  store sto;
  return evaluate(e, sto);
}
