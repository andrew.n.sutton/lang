
#include "ast.hpp"

#include <cassert>
#include <iostream>

const char*
get_spelling(arith_op op)
{
  switch (op) {
  case op_add: return "+";
  case op_sub: return "-";
  case op_mul: return "*";
  case op_div: return "/";
  case op_rem: return "%";
  case op_and: return "&";
  case op_ior: return "|";
  case op_xor: return "^";
  case op_shl: return "<<";
  case op_shr: return ">>";
  }
}

const char*
get_spelling(rel_op op)
{
  switch (op) {
  case op_eq: return "==";
  case op_ne: return "!=";
  case op_lt: return "<";
  case op_gt: return ">";
  case op_le: return "<=";
  case op_ge: return ">=";
  }
}

bool
is_value(const expr* e)
{
  switch (e->kind) {
  default:
    return false;
  case ek_abs:
  case ek_unit:
  case ek_bool:
  case ek_int:
  case ek_tuple:
  case ek_ph:
  case ek_fn:
    return true;
  }
}

bool
is_lambda(const expr* e)
{
  return e->kind == ek_abs || e->kind == ek_fn;
}

bool
is_object_type(const type* t)
{
  return t->kind != tk_ref;
}

bool
is_pointer_type(const type* t)
{
  return t->kind == tk_ptr;
}

bool
is_reference_type(const type* t)
{
  return t->kind == tk_ref;
}

// -------------------------------------------------------------------------- //
// Scope

/// FIXME: Deprecate this function.
var*
scope::declare(const std::string& s) 
{
  return declare(s, nullptr);
}

var*
scope::declare(const std::string& s, type* t) 
{
  assert(count(s) == 0);
  var* v = new var(s, t);
  emplace(s, v);  
  return v;
}

var*
scope::lookup(const std::string& s)
{
  auto iter = find(s);
  if (iter != end())
    return iter->second;
  else
    return nullptr;
}

/// FIXME: Deprecate this function.
var*
environment::declare(const std::string& s) 
{
  return back().declare(s, nullptr);
}

var*
environment::declare(const std::string& s, type* t) 
{
  return back().declare(s, t);
}

var*
environment::lookup(const std::string& s)
{
  for (auto iter = rbegin(); iter != rend(); ++iter) {
    if (var* v = iter->lookup(s))
      return v;
  } 
  return nullptr;
}

// -------------------------------------------------------------------------- //
// Location

bool
operator==(const static_location& a, const static_location& b)
{
  return a.index == b.index;
}

bool
operator!=(const static_location& a, const static_location& b)
{
  return !(a == b);
}

bool
operator==(const auto_location& a, const auto_location& b)
{
  return a.frame == b.frame && a.index == b.index;

}

bool
operator!=(const auto_location& a, const auto_location& b)
{
  return !(a == b);
}

bool
operator==(const free_location& a, const free_location& b)
{
  return a.index == b.index;
}

bool
operator!=(const free_location& a, const free_location& b)
{
  return !(a == b);
}

bool 
operator==(const location& a, const location& b)
{
  if (a.kind != b.kind) 
    return false;
  switch (a.kind) {
  case sk_null:
    return true;
  case sk_static:
    return a.sl == b.sl && a.path == b.path;
  case sk_auto:
    return a.al == b.al && a.path == b.path;
  case sk_free:
    return a.fl == b.fl && a.path == b.path;
  }
}

bool 
operator!=(const location& a, const location& b)
{
  return !(a == b);
}

// -------------------------------------------------------------------------- //
// Value

// Copy the representation of v into rep.
static void 
copy(value& dst, const value& src)
{
  dst.kind = src.kind;
  switch (dst.kind) {
  case vk_undef:
    new (&dst.rep.undef) undef_value(src.rep.undef);
    break;
  case vk_unit:
    new (&dst.rep.unit) unit_value(src.rep.unit);
    break;
  case vk_int:
    new (&dst.rep.num) int_value(src.rep.num);
    break;
  case vk_abs:
  case vk_fn:
    new (&dst.rep.ex) expr_value(src.rep.ex);
    break;
  case vk_agg:
    new (&dst.rep.agg) agg_value(src.rep.agg);
    break;
  case vk_loc:
    new (&dst.rep.loc) loc_value(src.rep.loc);
    break;
  }
}

/// Move the representation of v into rep.
static void
move(value& dst, value&& src)
{
  dst.kind = src.kind;
  switch (dst.kind) {
  case vk_undef:
    new (&dst.rep.undef) undef_value(std::move(src.rep.undef));
    break;
  case vk_unit:
    new (&dst.rep.unit) unit_value(std::move(src.rep.unit));
    break;
  case vk_int:
    new (&dst.rep.num) int_value(std::move(src.rep.num));
    break;
  case vk_abs:
  case vk_fn:
    break;
    new (&dst.rep.ex) expr_value(std::move(src.rep.ex));
  case vk_agg:
    new (&dst.rep.agg) agg_value(std::move(src.rep.agg));
    break;
  case vk_loc:
    new (&dst.rep.loc) loc_value(std::move(src.rep.loc));
    break;
  }
}

// Destroy the representation of v.
static void 
destroy(value& v)
{
  switch (v.kind) {
  case vk_undef:
    v.rep.undef.~undef_value();
    break;
  case vk_unit:
    v.rep.unit.~unit_value();
    break;
  case vk_int:
    v.rep.num.~int_value();
    break;
  case vk_abs:
  case vk_fn:
    v.rep.ex.~expr_value();
    break;
  case vk_agg:
    v.rep.agg.~agg_value();
    break;
  case vk_loc:
    v.rep.loc.~loc_value();
    break;
  }
}

value::value(const value& v)
{
  copy(*this, v);
}

value::value(value&& v)
{
  move(*this, std::move(v));
}

value::~value()
{
  destroy(*this);
}

value& 
value::operator=(const value& v)
{
  if (this != &v) {
    destroy(*this);
    copy(*this, v);
  }
  return *this;
}

value& 
value::operator=(value&& v)
{
  if (this != &v) {
    destroy(*this);
    move(*this, std::move(v));
  }
  return *this;
}

bool
operator==(const value& a, const value& b)
{
  if (a.kind != b.kind)
    return false;
  switch (a.kind) {
  case vk_undef:
  case vk_unit:
    return true;
  case vk_int:
    return a.rep.num == b.rep.num;
  case vk_abs:
  case vk_fn:
    // NOTE: Compare equal only for identical expressions. Equality and
    // equivalence are not typically valid for e.g., C++-like languages
    // since function values reduce to pointers.
    return a.rep.ex == b.rep.ex;
  case vk_agg:
    return a.rep.agg == b.rep.agg;
  case vk_loc:
    return a.rep.loc == b.rep.loc;
  }
}

bool
operator!=(const value& a, const value& b)
{
  return !(a == b);
}


// -------------------------------------------------------------------------- //
// Stores

location
static_store::allocate(const var* x)
{
  static_location loc = objs.size();
  objs.emplace_back();
  locs.emplace(x, loc);
  return loc;
}

location
static_store::lookup(const var* x)
{
  auto iter = locs.find(x);
  if (iter != locs.end())
    return iter->second;
  else
    return {};
}

location
frame::allocate()
{
  auto_location loc(depth, objs.size());
  objs.emplace_back();
  return loc;
}

location
frame::allocate(const var* x)
{
  location loc = allocate();
  locs.emplace(x, loc);
  return loc;
}

location
frame::lookup(const var* x)
{
  auto iter = locs.find(x);
  if (iter != locs.end())
    return iter->second;
  else
    return {};
}

location
automatic_store::allocate()
{
  assert(!empty());
  return back().allocate();
}

location
automatic_store::allocate(const var* x)
{
  assert(!empty());
  return back().allocate(x);
}

location
automatic_store::lookup(const var* x)
{
  for (auto iter = rbegin(); iter != rend(); ++iter) {
    if (location loc = iter->lookup(x))
      return loc;
  }
  return {};
}

location
free_store::allocate()
{
  if (free.empty()) {
    // Add a new object to store.
    free_location loc(objs.size());
    objs.emplace_back();
    return loc;
  } else {
    // Reuse the location of an existing object.
    free_location loc(free.top());
    free.pop();
    return loc;
  }
}

// Resets the the value of the object and adds the location to
// the free list.
//
// Note that deallocation only applies to complete objects.
void
free_store::deallocate(location loc)
{
  assert(loc.kind == sk_free); 
  assert(loc.path.empty()); 
  int ix = loc.fl.index;
  objs[ix] = value();
  free.push(ix);
}

value&
store::resolve(location loc)
{
  switch (loc.kind) {
  case sk_null:
    throw std::logic_error("resolving null location");
  case sk_static:
    return globals.objs[loc.sl.index];
  case sk_auto:
    return stack[loc.al.frame].objs[loc.al.index];
  case sk_free:
    return free.objs[loc.al.index];
  }
}
