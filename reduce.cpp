
#include "ast.hpp"

#include <cassert>

// Substitute the argument through the abstraction.
expr*
apply(abs_expr* abs, expr* arg)
{
  subst s {
    {abs->parm, arg}
  };
  return substitute(abs->body, s);
}

// Substitute the argument through the abstraction.
expr*
apply(fn_expr* abs, expr_seq& args)
{
  assert(abs->parms.size() == args.size());
  
  // Build a substitution of each argument for its corresponding parameter.
  subst s;
  auto pi = abs->parms.begin(); 
  auto ai = args.begin();
  while (pi != abs->parms.end()) {
    s.emplace(*pi, *ai);
    ++pi;
    ++ai;
  }
  
  return substitute(abs->body, s);
}

// Substitute the argument through the abstraction.
expr*
partial_apply(fn_expr* abs, expr_seq& args)
{
  assert(abs->parms.size() == args.size());
  
  // Build a substitution of each non-placeholder argument for its 
  // corresponding parameter.
  var_seq parms;
  subst sub;
  auto pi = abs->parms.begin(); 
  auto ai = args.begin();
  while (pi != abs->parms.end()) {
    expr* a = *ai;
    if ((*ai)->kind != ek_ph)
      sub.emplace(*pi, *ai);
    else
      parms.push_back(*pi);
    ++pi;
    ++ai;
  }

  // Rename (and retain) the original variables so that we don't
  // have any name collisions.
  environment env;
  scope_guard g(env);
  for (var*& v : parms)
    v = env.declare(v->name);

  // Substitute through the body.
  expr* body = substitute(abs->body, sub, env);

  // Rebuild the expression.
  return new fn_expr(std::move(parms), body);
}


expr*
reduce_app(app_expr* e)
{
  if (!is_value(e->abs)) { // App-1
    expr* abs = reduce(e->abs);
    return new app_expr(abs, e->arg);
  }
  if (!is_value(e->arg)) { // App-2
    expr* arg = reduce(e->arg);
    return new app_expr(e->abs, arg);
  }
  
  // Make sure the abstraction is actually an abstraction.
  if (e->abs->kind != ek_abs)
    throw std::logic_error("application to non-abstraction");
  abs_expr *abs = static_cast<abs_expr*>(e->abs);
  
  return apply(abs, e->arg);
}

expr*
reduce_cond(cond_expr* e)
{
  if (!is_value(e->cond)) {
    expr* cond = reduce(e->cond);
    return new cond_expr(cond, e->tval, e->fval);
  }

  if (e->cond->kind != ek_bool)
    throw std::logic_error("condition is not boolean");
  bool_expr* b = static_cast<bool_expr*>(e->cond);

  if (b->val)
    return e->tval;
  else
    return e->fval;
}

expr*
reduce_call(call_expr* e)
{
  if (!is_value(e->abs)) { // App-1
    expr* abs = reduce(e->abs);
    return new call_expr(abs, e->args);
  }
  
  // Perform a one-step reduction of the first non-value argument.
  auto iter = std::find_if_not(e->args.begin(), e->args.end(), is_value);
  if (iter != e->args.end()) {
    expr_seq args(e->args.size());
    auto pos = std::copy(e->args.begin(), iter, args.begin());
    *pos = reduce(*iter);
    std::copy(++iter, e->args.end(), ++pos);
    return new call_expr(e->abs, std::move(args));
  }

  // Make sure the abstraction is actually an abstraction.
  if (e->abs->kind != ek_fn)
    throw std::logic_error("application to non-function");
  fn_expr *abs = static_cast<fn_expr*>(e->abs);

  // Make sure the number of arguments is in agreement.
  if (abs->parms.size() < e->args.size())
    throw std::runtime_error("too many arguments");
  if (abs->parms.size() > e->args.size())
    throw std::runtime_error("too few arguments");

  return apply(abs, e->args);
}

// FIXME: There's a lot of code shared between this and call expressions.
expr*
reduce_bind(bind_expr* e)
{
  if (!is_value(e->abs)) { // App-1
    expr* abs = reduce(e->abs);
    return new call_expr(abs, e->args);
  }
  
  // Perform a one-step reduction of the first non-value argument.
  auto iter = std::find_if_not(e->args.begin(), e->args.end(), is_value);
  if (iter != e->args.end()) {
    expr_seq args(e->args.size());
    auto pos = std::copy(e->args.begin(), iter, args.begin());
    *pos = reduce(*iter);
    std::copy(++iter, e->args.end(), ++pos);
    return new call_expr(e->abs, std::move(args));
  }

  // Make sure the abstraction is actually an abstraction.
  if (e->abs->kind != ek_fn)
    throw std::logic_error("application to non-fn");
  fn_expr *abs = static_cast<fn_expr*>(e->abs);

  // Make sure the number of arguments is in agreement.
  if (abs->parms.size() < e->args.size())
    throw std::runtime_error("too many arguments");
  if (abs->parms.size() > e->args.size())
    throw std::runtime_error("too few arguments");

  // Build the new body by substituting through.
  return partial_apply(abs, e->args);
}

expr*
reduce(expr* e)
{
  switch (e->kind) {
    default:
      throw std::logic_error("invalid reduction");
    case ek_app:
      return reduce_app(static_cast<app_expr*>(e));
    case ek_cond:
      return reduce_cond(static_cast<cond_expr*>(e));
    case ek_call:
      return reduce_call(static_cast<call_expr*>(e));
    case ek_bind:
      return reduce_bind(static_cast<bind_expr*>(e));
  }
}
