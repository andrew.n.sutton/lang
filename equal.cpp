
#include "ast.hpp"

// FIXME: Is this right?
bool
equal(const var* a, const var* b)
{
  return a->name == b->name;
}

bool
equal(const var_seq& a, const var_seq& b)
{
  return std::equal(a.begin(), a.end(), b.begin(), b.end(), 
    [](const var* x, const var* y) {
      return equal(x, y);
    });
}

// -------------------------------------------------------------------------- //
// Equality (expressions)

bool
equal(const expr_seq& a, const expr_seq& b)
{
  return std::equal(a.begin(), a.end(), b.begin(), b.end(), 
    [](const expr* x, const expr* y) {
      return equal(x, y);
    });
}

bool
equal_ref_exprs(const ref_expr* e1, const ref_expr* e2)
{
  return equal(e1->ref, e2->ref);
}

bool
equal_abs_exprs(const abs_expr* e1, const abs_expr* e2)
{
  return equal(e1->parm, e2->parm) 
      && equal(e1->body, e2->body);
}

bool
equal_app_exprs(const app_expr* e1, const app_expr* e2)
{
  return equal(e1->abs, e2->abs) 
      && equal(e1->arg, e2->arg);
}

bool
equal_unit_exprs(const unit_expr* e1, const unit_expr* e2)
{
  return true;
}

bool
equal_bool_exprs(const bool_expr* e1, const bool_expr* e2)
{
  return e1->val == e2->val;
}

bool
equal_cond_exprs(const cond_expr* e1, const cond_expr* e2)
{
  return equal(e1->cond, e2->cond) 
      && equal(e1->tval, e2->tval) 
      && equal(e1->fval, e2->fval);
}

bool
equal_and_exprs(const and_expr* e1, const and_expr* e2)
{
  return equal(e1->lhs, e2->lhs) 
      && equal(e1->rhs, e2->rhs);
}

bool
equal_or_exprs(const or_expr* e1, const or_expr* e2)
{
  return equal(e1->lhs, e2->lhs) 
      && equal(e1->rhs, e2->rhs);
}

bool
equal_not_exprs(const not_expr* e1, const not_expr* e2)
{
  return equal(e1->arg, e2->arg);
}

bool
equal_int_exprs(const int_expr* e1, const int_expr* e2)
{
  return e1->val == e2->val;
}

bool
equal_arith_exprs(const arith_expr* e1, const arith_expr* e2)
{
  return e1->op == e2->op 
      && equal(e1->lhs, e2->lhs) 
      && equal(e1->rhs, e2->rhs);
}

bool
equal_rel_exprs(const rel_expr* e1, const rel_expr* e2)
{
  return e1->op == e2->op 
      && equal(e1->lhs, e2->lhs) 
      && equal(e1->rhs, e2->rhs);
}

bool
equal_tuple_exprs(const tuple_expr* e1, const tuple_expr* e2)
{
  return equal(e1->elems, e2->elems);
}

bool
equal_proj_exprs(const proj_expr* e1, const proj_expr* e2)
{
  return e1->elem == e2->elem && equal(e1->base, e2->base);
}

bool
equal_new_exprs(const new_expr* e1, const new_expr* e2)
{
  return equal(e1->ty, e2->ty) && equal(e1->init, e2->init);
}

bool
equal_del_exprs(const del_expr* e1, const del_expr* e2)
{
  return equal(e1->obj, e2->obj);
}

bool
equal_addr_exprs(const addr_expr* e1, const addr_expr* e2)
{
  return equal(e1->obj, e2->obj);
}

bool
equal_deref_exprs(const deref_expr* e1, const deref_expr* e2)
{
  return equal(e1->ptr, e2->ptr);
}

bool
equal_load_exprs(const load_expr* e1, const load_expr* e2)
{
  return equal(e1->obj, e2->obj);
}

bool
equal_store_exprs(const store_expr* e1, const store_expr* e2)
{
  return equal(e1->obj, e2->obj) && equal(e1->val, e2->val);
}

bool
equal_fn_exprs(const fn_expr* e1, const fn_expr* e2)
{
  return equal(e1->parms, e2->parms) 
      && equal(e1->body, e2->body);
}

bool
equal_call_exprs(const call_expr* e1, const call_expr* e2)
{
  return equal(e1->abs, e2->abs) 
      && equal(e1->args, e2->args);
}

bool
equal_ph_exprs(const ph_expr* e1, const ph_expr* e2)
{
  return true;
}

bool
equal_bind_exprs(const bind_expr* e1, const bind_expr* e2)
{
  return equal(e1->abs, e2->abs) 
      && equal(e1->args, e2->args);
}

bool
equal_let_exprs(const let_expr* e1, const let_expr* e2)
{
  return equal(e1->parm, e2->parm) 
      && equal(e1->init, e2->init) 
      && equal(e1->body, e2->body);
}

bool
equal_seq_exprs(const seq_expr* e1, const seq_expr* e2)
{
  return equal(e1->first, e2->first) 
      && equal(e1->second, e2->second);
}

bool
equal(const expr* e1, const expr* e2)
{
  if (e1 == e2)
    return true;
  if (e1->kind != e2->kind)
    return false;
  switch (e1->kind) {
#define def_expr(E) \
  case ek_ ## E: \
    return equal_ ## E ## _exprs((const E ## _expr*)e1, (const E ## _expr*)e2);
#include "expr.def"
  }
}

bool 
equal(const type_seq& a, const type_seq& b)
{
  return std::equal(a.begin(), a.end(), b.begin(), b.end(), [](const type* x, const type* y) {
    return equal(x, y);
  });
}

// -------------------------------------------------------------------------- //
// Equality (types)

bool
equal_unit_types(const unit_type* t1, const unit_type* t2)
{
  return true;
}

bool
equal_bool_types(const bool_type* t1, const bool_type* t2)
{
  return true;
}

bool
equal_int_types(const int_type* t1, const int_type* t2)
{
  return true;
}

/// FIXME: Is this right? When are two placeholder types equal?
bool
equal_ph_types(const ph_type* t1, const ph_type* t2)
{
  return true;
}

bool
equal_arrow_types(const arrow_type* t1, const arrow_type* t2)
{
  return equal(t1->parm, t2->parm) 
      && equal(t1->ret, t2->ret);
}

bool
equal_fn_types(const fn_type* t1, const fn_type* t2)
{
  return equal(t1->parms, t2->parms) 
      && equal(t1->ret, t2->ret);
}

bool
equal_tuple_types(const tuple_type* t1, const tuple_type* t2)
{
  return equal(t1->elems, t2->elems);  
}

bool
equal(const field* a, const field* b)
{
  return a->id == b->id && equal(a->ty, b->ty);
}

bool
equal(const field_seq& a, const field_seq& b)
{
  return std::equal(a.begin(), a.end(), b.begin(), b.end(), [](const field* x, const field* y) {
    return equal(x, y);
  });
}

bool
equal_record_types(const record_type* t1, const record_type* t2)
{
  return equal(t1->fields, t2->fields);  
}

bool
equal_variant_types(const variant_type* t1, const variant_type* t2)
{
  return equal(t1->fields, t2->fields);  
}

bool
equal_ptr_types(const ptr_type* t1, const ptr_type* t2)
{
  return equal(t1->elem, t2->elem);
}

bool
equal_ref_types(const ref_type* t1, const ref_type* t2)
{
  return equal(t1->obj, t2->obj);
}

bool
equal(const type* t1, const type* t2)
{
  if (t1->kind != t2->kind)
    return false;
  switch (t1->kind) {
#define def_type(T) \
  case tk_ ## T: \
    return equal_ ## T ## _types((const T ## _type*)t1, (const T ## _type*)t2);
#include "type.def"
  }
}

// -------------------------------------------------------------------------- //
// Alpha equivalence (expressions)

/// A record of equivalent variables in a lexical context. This is a bijection
/// on variables.
///
/// This does not maintain a full equivalence closure. The only way for
/// variables to be in correspondence is to be declared in the same positions
/// in an expression.
///
/// The term "contour" is a more general term for a scope or frame in a
/// lexical or dynamic binding environment.
struct contour : std::unordered_map<const var*, const var*>
{
  const var* get(const var* v) const
  {
    auto iter = find(v);
    if (iter != end())
      return iter->second;
    else
      return nullptr;
  }

  void equate(const var* v1, const var* v2)
  {
    emplace(v1, v2);
    emplace(v2, v1);
  }
};

/// A stack of equivalence contours.
struct equivalence : std::vector<contour>
{
  /// Enter a new scope.
  void enter_scope() { emplace_back(); }

  /// Leave a scope.
  void leave_scope() { pop_back(); }

  /// Returns the variable corresponding to v or nullptr if none.
  const var* get(const var* v) const
  {
    for (auto iter = rbegin(); iter != rend(); ++iter)
      if (const var* x = iter->get(v))
        return x;
    return nullptr;
  }

  // Returns true if (v1, v2) is in the relation.
  bool contains(const var* v1, const var* v2) const
  {
    // Equivalent to get(v2) == v1;
    return get(v1) == v2;
  }

  // Adds (v1, v2) and (v2, v1) to the relation.
  void equate(const var* v1, const var* v2)
  {
    return back().equate(v1, v2);
  }
};

/// A helper class for managing nested contours.
struct nested_equivalence_scope
{
  nested_equivalence_scope(equivalence& eq)
    : eq(eq)
  {
    eq.enter_scope();
  }
  ~nested_equivalence_scope()
  {
    eq.leave_scope();
  }

  equivalence& eq;
};

bool alpha_eq(const expr* e1, const expr* e2, equivalence& eq);

bool 
alpha_eq(const expr_seq& a, const expr_seq& b, equivalence& eq)
{
  return std::equal(a.begin(), a.end(), b.begin(), 
    [&eq](const expr* x, const expr* y) {
      return alpha_eq(x, y, eq);
    });
}

bool
alpha_eq_ref_exprs(const ref_expr* e1, const ref_expr* e2, equivalence& eq)
{
  return eq.contains(e1->ref, e2->ref);
}

bool
alpha_eq_abs_exprs(const abs_expr* e1, const abs_expr* e2, equivalence& eq)
{
  nested_equivalence_scope scope(eq);
  eq.equate(e1->parm, e2->parm);
  return alpha_eq(e1->body, e2->body, eq);
}

bool
alpha_eq_app_exprs(const app_expr* e1, const app_expr* e2, equivalence& eq)
{
  return alpha_eq(e1->abs, e2->abs, eq) &&
         alpha_eq(e1->arg, e2->arg, eq);
}

bool
alpha_eq_unit_exprs(const unit_expr* e1, const unit_expr* e2, equivalence& eq)
{
  return true;
}

bool
alpha_eq_bool_exprs(const bool_expr* e1, const bool_expr* e2, equivalence& eq)
{
  return e1->val == e2->val;
}

bool
alpha_eq_cond_exprs(const cond_expr* e1, const cond_expr* e2, equivalence& eq)
{
  return alpha_eq(e1->cond, e2->cond, eq) 
      && alpha_eq(e1->tval, e2->tval, eq) 
      && alpha_eq(e1->fval, e2->fval, eq);
}

bool
alpha_eq_and_exprs(const and_expr* e1, const and_expr* e2, equivalence& eq)
{
  return alpha_eq(e1->lhs, e2->lhs, eq)
      && alpha_eq(e1->rhs, e2->rhs, eq);
}


bool
alpha_eq_or_exprs(const or_expr* e1, const or_expr* e2, equivalence& eq)
{
  return alpha_eq(e1->lhs, e2->lhs, eq) 
      && alpha_eq(e1->rhs, e2->rhs, eq);
}

bool
alpha_eq_not_exprs(const not_expr* e1, const not_expr* e2, equivalence& eq)
{
  return alpha_eq(e1->arg, e2->arg, eq);
}

bool
alpha_eq_int_exprs(const int_expr* e1, const int_expr* e2, equivalence& eq)
{
  return e1->val == e2->val;
}

bool
alpha_eq_arith_exprs(const arith_expr* e1, const arith_expr* e2, equivalence& eq)
{
  return e1->op == e2->op 
      && alpha_eq(e1->lhs, e2->lhs, eq) 
      && alpha_eq(e1->rhs, e2->rhs, eq);
}

bool
alpha_eq_rel_exprs(const rel_expr* e1, const rel_expr* e2, equivalence& eq)
{
  return e1->op == e2->op 
      && alpha_eq(e1->lhs, e2->lhs, eq) 
      && alpha_eq(e1->rhs, e2->rhs, eq);
}

bool
alpha_eq_tuple_exprs(const tuple_expr* e1, const tuple_expr* e2, equivalence& eq)
{
  return alpha_eq(e1->elems, e2->elems, eq);
}

bool
alpha_eq_proj_exprs(const proj_expr* e1, const proj_expr* e2, equivalence& eq)
{
  return e1->elem == e2->elem
      && alpha_eq(e1->base, e2->base, eq);
}

// Note that the type comparison for equality, not alpha equivalence.
bool
alpha_eq_new_exprs(const new_expr* e1, const new_expr* e2, equivalence& eq)
{
  return equal(e1->ty, e2->ty) && alpha_eq(e1->init, e2->init, eq);
}

bool
alpha_eq_del_exprs(const del_expr* e1, const del_expr* e2, equivalence& eq)
{
  return alpha_eq(e1->obj, e2->obj, eq);
}

bool
alpha_eq_addr_exprs(const addr_expr* e1, const addr_expr* e2, equivalence& eq)
{
  return alpha_eq(e1->obj, e2->obj, eq);
}

bool
alpha_eq_deref_exprs(const deref_expr* e1, const deref_expr* e2, equivalence& eq)
{
  return alpha_eq(e1->ptr, e2->ptr, eq);
}

bool
alpha_eq_load_exprs(const load_expr* e1, const load_expr* e2, equivalence& eq)
{
  return alpha_eq(e1->obj, e2->obj, eq);
}

bool
alpha_eq_store_exprs(const store_expr* e1, const store_expr* e2, equivalence& eq)
{
  return alpha_eq(e1->obj, e2->obj, eq) && alpha_eq(e1->val, e2->val, eq);
}

bool
alpha_eq_fn_exprs(const fn_expr* e1, const fn_expr* e2, equivalence& eq)
{
  if (e1->parms.size() != e2->parms.size())
    return false;

  nested_equivalence_scope scope(eq);
  auto iter1 = e1->parms.begin();
  auto iter2 = e2->parms.begin();
  while (iter1 != e1->parms.end()) {
    eq.equate(*iter1, *iter2);
    ++iter1;
    ++iter2;
  }

  return alpha_eq(e1->body, e2->body, eq);
}

bool
alpha_eq_call_exprs(const call_expr* e1, const call_expr* e2, equivalence& eq)
{
  return alpha_eq(e1->abs, e2->abs, eq) 
      && alpha_eq(e1->args, e2->args, eq);
}

bool
alpha_eq_ph_exprs(const ph_expr* e1, const ph_expr* e2, equivalence& eq)
{
  return true;
}

bool
alpha_eq_bind_exprs(const bind_expr* e1, const bind_expr* e2, equivalence& eq)
{
  return alpha_eq(e1->abs, e2->abs, eq) && alpha_eq(e1->args, e2->args, eq);
}

bool
alpha_eq_let_exprs(const let_expr* e1, const let_expr* e2, equivalence& eq)
{
  nested_equivalence_scope scope(eq);
  eq.equate(e1->parm, e2->parm);
  return alpha_eq(e1->init, e2->init, eq) && alpha_eq(e1->body, e2->body, eq);
}

bool
alpha_eq_seq_exprs(const seq_expr* e1, const seq_expr* e2, equivalence& eq)
{
  return alpha_eq(e1->first, e2->first, eq) && alpha_eq(e1->second, e2->second, eq);
}

bool
alpha_eq(const expr* e1, const expr* e2, equivalence& eq)
{
  if (e1 == e2)
    return true;
  if (e1->kind != e2->kind)
    return false;
  switch (e1->kind) {
#define def_expr(E) \
  case ek_ ## E: \
    return alpha_eq_ ## E ## _exprs((const E ## _expr*)e1, (const E ## _expr*)e2, eq);
#include "expr.def"
  }
}

bool
alpha_equivalent(const expr* e1, const expr* e2)
{
  equivalence eq;
  return alpha_eq(e1, e2, eq);
}

// -------------------------------------------------------------------------- //
// Beta equivalence

/// FIXME: Evaluating two expressions is the easiest way to compute this, but
/// maybe not the best.
bool 
beta_equivalent(const expr* e1, const expr* e2)
{
  return evaluate(e1) == evaluate(e2);
}

