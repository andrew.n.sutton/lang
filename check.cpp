
#include "ast.hpp"

/// This file implements type checking.
///
/// Note that we don't have to maintain any extra context for the type
/// checking algorithm because all references are correctly bound by the
/// time this algorithm runs. See the comments on typecheck_abs_expr for
/// details.

/// Just return the type of the variable.
type*
typecheck(var* x)
{
  return x->ty;
}

/// x : T in G   object(T) is true
/// ------------------------------ [Type-Ref-Object]
///         G |- x : T&
///
/// x : T& in G   reference(T) is true
/// ---------------------------------- [Type-Ref-Reference]
///         G |- x : T&
type*
typecheck_ref_expr(ref_expr* e)
{
  type* ref = typecheck(e->ref);
  if (is_reference_type(ref))
    return ref;
  else
    return new ref_type(ref);
}

/// G, x : T1 |- e : T2
/// ------------------- [Type-Abs]
///  G |- \x:T1.e : T2
///
/// NOTE: We don't need to add the typing x : T to the context before
/// typing the body of the abstraction. Any references in e to x will
/// be bound to the correct variable and have the appropriate type.
type*
typecheck_abs_expr(abs_expr* e)
{
  type* parm = typecheck(e->parm);
  type* ret = typecheck(e->body);
  return new arrow_type(parm, ret);
}

/// G |- e1 : T1->T2   G |- e2 : T2
/// ------------------------------- [Type-App]
///        G |- e1 e2 : T2
type*
typecheck_app_expr(app_expr* e)
{
  type* abs = typecheck(e->abs);
  type* arg = typecheck(e->arg);
  if (abs->kind != tk_arrow)
    throw std::runtime_error("not an abstraction");
  arrow_type* arr = static_cast<arrow_type*>(abs);
  if (!equal(arr->parm, arg))
    throw std::runtime_error("argument mismatch");
  return arr->ret;
}

/// ---------------- [Type-Unit]
/// G |- unit : Unit
type*
typecheck_unit_expr(unit_expr* e)
{
  return new unit_type();
}

/// ---------------- [Type-Bool]
/// G |- bool : Bool
type*
typecheck_bool_expr(bool_expr* e)
{
  return new bool_type();
}

/// G |- e1 : bool   G |- e2 : T   G |- e3 : T
/// ------------------------------------------ [Type-Cond]
///       G |- if e1 then e2 else e3 : T
type*
typecheck_cond_expr(cond_expr* e)
{
  type* cond = typecheck(e->cond);
  type* tval = typecheck(e->tval);
  type* fval = typecheck(e->fval);
  if (cond->kind != tk_bool)
    throw std::runtime_error("condition is not booolean");
  if (!equal(tval, fval))
    throw std::runtime_error("conditional type mismatch");
  return tval;
}

/// G |- e1 : bool   G |- e2 : bool
/// ------------------------------- [Type-And]
///     G |- e1 and e2 : bool
type*
typecheck_and_expr(and_expr* e)
{
  type* lhs = typecheck(e->lhs);
  type* rhs = typecheck(e->rhs);
  if (lhs->kind != tk_bool)
    throw std::runtime_error("operand is not boolean");
  if (rhs->kind != tk_bool)
    throw std::runtime_error("operand is not boolean");
  return new bool_type();
}

/// G |- e1 : bool   G |- e2 : bool
/// ------------------------------- [Type-Or]
///      G |- e1 or e2 : bool
type*
typecheck_or_expr(or_expr* e)
{
  type* lhs = typecheck(e->lhs);
  type* rhs = typecheck(e->rhs);
  if (lhs->kind != tk_bool)
    throw std::runtime_error("operand is not boolean");
  if (rhs->kind != tk_bool)
    throw std::runtime_error("operand is not boolean");
  return new bool_type();
}

///   G |- e1 : bool
/// ------------------ [Type-Not]
/// G |- not e1 : bool
type*
typecheck_not_expr(not_expr* e)
{
  type* arg = typecheck(e->arg);
  if (arg->kind != tk_bool)
    throw std::runtime_error("operand is not boolean");
  return new bool_type();
}

/// ------------ [Type-Int]
/// G |- n : int
type*
typecheck_int_expr(int_expr* e)
{
  return new int_type();
}

/// G |- e1 : int G |- e2 : int
/// --------------------------- [Type-Not]
///     G |- e1 @ e2 : int
type*
typecheck_arith_expr(arith_expr* e)
{
  type* lhs = typecheck(e->lhs);
  type* rhs = typecheck(e->rhs);
  if (lhs->kind != tk_int)
    throw std::runtime_error("operand is not integral");
  if (rhs->kind != tk_int)
    throw std::runtime_error("operand is not integral");
  return new int_type();
}

/// G |- e1 : int G |- e2 : int
/// --------------------------- [Type-Not]
///     G |- e1 @ e2 : bool
type*
typecheck_rel_expr(rel_expr* e)
{
  type* lhs = typecheck(e->lhs);
  type* rhs = typecheck(e->rhs);
  if (lhs->kind != tk_int)
    throw std::runtime_error("operand is not integral");
  if (rhs->kind != tk_int)
    throw std::runtime_error("operand is not integral");
  return new bool_type();
}

//    for each i in [1, n], G |- ei : Ti
// ------------------------------------------ [Type-Tuple]
// G |- {e1, e2, ..., en} : {T1, T2, ..., Tn}
type*
typecheck_tuple_expr(tuple_expr* e)
{
  type_seq elems;
  for (expr* x : e->elems)
    elems.push_back(typecheck(e));
  return new tuple_type(std::move(elems));
}

/// G |- e : {T1, T2, ..., Tk}
/// -------------------------- [Type-Proj]
///      G |- e.n : Tn
type*
typecheck_proj_expr(proj_expr* e)
{
  type* base = typecheck(e->base);
  if (base->kind != tk_tuple)
    throw std::runtime_error("base object is not a tuple");;
  tuple_type* tup = (tuple_type*)base;
  if (e->elem < 0)
    throw std::logic_error("negative projection index");
  if (e->elem >= tup->elems.size())
    throw std::runtime_error("projected element out of bounds");
  return tup->elems[e->elem];
}

///    G |- e : T
/// ----------------- [Type-New]
/// G |- new T e : T*
type*
typecheck_new_expr(new_expr* e)
{
  type* init = typecheck(e->init);
  if (!equal(init, e->ty))
    throw std::runtime_error("type mismatch in new initializer");
  return new ptr_type(init);
}

///     G |- e : T*
/// -------------------- [Type-Del]
/// G |- delete e : unit
///
/// NOTE: This is not a typesafe operation since we do not differentiate
/// between locations arising from different stores. In other words, the
/// typing rules would allow deletion of free store, automatic store,
/// and static store objects. That's not great.
type*
typecheck_del_expr(del_expr* e)
{
  type* obj = typecheck(e->obj);
  if (!is_pointer_type(obj))
    throw std::runtime_error("deleting non-pointer");
  return new unit_type();
}

/// G |- e : T&
/// ------------ [Type-Addr]
/// G |- &e : T*
type*
typecheck_addr_expr(addr_expr* e)
{
  type* obj = typecheck(e->obj);
  if (!is_reference_type(obj))
    throw std::runtime_error("address of non-reference");
  ref_type* ref = (ref_type*)obj;
  return new ptr_type(ref->obj);
}

/// G |- e : T*
/// ------------ [Type-Deref]
/// G |- *e : T&
type*
typecheck_deref_expr(deref_expr* e)
{
  type* ptr = typecheck(e->ptr);
  if (!is_pointer_type(ptr))
    throw std::runtime_error("dereference of non-pointer");
  ptr_type* t1 = (ptr_type*)ptr;
  return new ref_type(t1->elem);
}

/// G |- e : T&
/// ----------- [Type-Load]
/// G |- @e : T
type*
typecheck_load_expr(load_expr* e)
{
  type* obj = typecheck(e->obj);
  if (!is_reference_type(obj))
    throw std::runtime_error("load of non-reference");
  ref_type* t1 = (ref_type*)obj;
  return t1->obj;
}

/// G |- e1 : T1&   G |- e2 : T2
/// ---------------------------- [Type-Store]
///      G |- e1 = e2 : T1&
type*
typecheck_store_expr(store_expr* e)
{
  type* obj = typecheck(e->obj);
  type* val = typecheck(e->val);
  if (!is_reference_type(obj))
    throw std::runtime_error("store to non-reference");
  if (!is_object_type(val))
    throw std::runtime_error("store of non-object");
  return (ref_type*)obj;
}


///             G, x1:T1, x2:T2, ..., xn:Tn |- e : T
/// ----------------------------------------------------------- [Type-Lambda]
/// G |- \(x1:T1, x2:T2, ..., xn : Tn).e : (T1, T2, ..., Tn)->T
///
/// Note that we don't need to modify the context. We explicitly associate
/// the type with the variable on construction, and we guarantee the uniqueness
/// of names at the same time.
type*
typecheck_fn_expr(fn_expr* e)
{
  type_seq parms;
  for (var* x : e->parms)
    parms.push_back(typecheck(x));
  type* ret = typecheck(e->body);
  return new fn_type(std::move(parms), ret);
}

/// G |- e : (T1, T2, ..., Tn)->T  
///        G |- e1 : T1
///        G |- e2 : T2
///             ...
///        G |- en : Tn
/// ----------------------------
/// G |- e(e1, e2, ..., en) : T
type*
typecheck_call_expr(call_expr* e)
{
  type* abs = typecheck(e->abs);
  type_seq args;
  for (expr* a : e->args)
    args.push_back(typecheck(a));
  
  if (abs->kind != tk_fn)
    throw std::runtime_error("not a function");
  fn_type* fn = (fn_type*)abs;
  
  if (fn->parms.size() < args.size())
    throw std::runtime_error("too many arguments");
  if (fn->parms.size() > args.size())
    throw std::runtime_error("too few arguments");
  
  auto ai = args.begin();
  auto pi = fn->parms.begin();
  while (ai != args.end()) {
    if (!equal(*ai, *pi))
      throw std::runtime_error("argument mismatch");
      ++ai;
      ++pi;    
  }

  return fn->ret;
}

/// ---------- [Type-Ph]
/// G |- _ : ?
type*
typecheck_ph_expr(ph_expr* e)
{
  return new ph_type();
}


/// G |- e : (T1, T2, ..., Tn)->T
/// let pi be the subsequence of ei with type ? and
/// call the corresponding types Tp1, Tp2, ..., Tpk
/// ------------------------------------------------- [Type-Bind]
/// G |- e(e1, e2, ..., en) : (Tp1, Tp2, ..., Tpk)->T
///
/// FIXME: The formalism falls apart here.
///
/// FIXME: There's a lot of overlap with call expressions.
type*
typecheck_bind_expr(bind_expr* e)
{
  type* abs = typecheck(e->abs);
  
  type_seq args;
  for (expr* a : e->args)
    args.push_back(typecheck(a));
  
  if (abs->kind != tk_fn)
    throw std::runtime_error("not a function");
  fn_type* fn = (fn_type*)abs;
  
  if (fn->parms.size() < args.size())
    throw std::runtime_error("too many arguments");
  if (fn->parms.size() > args.size())
    throw std::runtime_error("too few arguments");
  
  // Match each argument against the corresponding parameter. The types
  // of parameters are "deduced" from the types of arguments.
  type_seq parms;
  auto ai = args.begin();
  auto pi = fn->parms.begin();
  while (ai != args.end()) {
    if ((*ai)->kind == tk_ph)
      parms.push_back(*pi);
    if (!equal(*ai, *pi))
      throw std::runtime_error("argument mismatch");
      ++ai;
      ++pi;
  }

  return new fn_type(std::move(parms), fn->ret);
}

type*
typecheck_let_expr(let_expr* e)
{
  type* parm = typecheck(e->parm);
  type* init = typecheck(e->init);
  type* body = typecheck(e->body);
  if (!equal(parm, init))
    throw std::runtime_error("type mismatch in let binding");
  return body;
}

type*
typecheck_seq_expr(seq_expr* e)
{
  type* first = typecheck(e->first);
  type* second = typecheck(e->second);
  return second;
}

type*
compute_type(expr* e)
{
  switch (e->kind) {
#define def_expr(E) \
  case ek_ ## E: \
    return typecheck_ ## E ## _expr((E ## _expr*)e);
#include "expr.def"
  }
}

type*
typecheck(expr* e)
{
  if (!e->ty)
    e->ty = compute_type(e);
  return e->ty;
}
