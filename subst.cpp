
#include "ast.hpp"

/// This file implements value substitution.
///
/// FIXME: What should we do with typing? Combine subst and types?

// Generate a unique 'name' for v by registering a new declaration.
var*
rename(var* v, environment& env)
{
  return env.declare(v->name);
}

// Find the replacement name for v. Throws an exception if the variable 
// is free.
var*
lookup(var* v, environment& env)
{
  var* r = env.lookup(v->name);
  if (!r)
    throw std::runtime_error("free variable");
  return r;
}

expr* substitute(expr* e, const subst& s, environment& env);

/// Replace the identifier with its substitution or a reference to
/// it's bound variable.
///
///     [x->s]x = s
///     [x->s]y = lookup(y)   if y != x
///
/// We need to perform a lookup in order to ensure that all variables have
/// unique names.
expr*
substitute_ref_expr(ref_expr* e, const subst& s, environment& env)
{
  auto iter = s.find(e->ref);
  if (iter != s.end())
    // Replace the reference with its substitution.
    return iter->second; 
  else  
    // Replace the reference with its renamed version.
    return new ref_expr(lookup(e->ref, env));
}

/// [x->s]\y.e = \rename(y).[x->s]e
///
/// We always rename y, which ensures that the substitution avoids capture.
/// In this context, renaming means creating a new variable and then replacing
/// references to it.
expr*
substitute_abs_expr(abs_expr* e, const subst& s, environment& env)
{
  scope_guard g(env);
  var* parm = rename(e->parm, env);
  expr* body = substitute(e->body, s, env);
  return new abs_expr(parm, body);
}

/// [x->s](e1 e2) = [x->s]e1 [x->s]e2
///
/// Apply substitution to both operands.
expr*
substitute_app_expr(app_expr* e, const subst& s, environment& env)
{
  expr* abs = substitute(e->abs, s, env);
  expr* arg = substitute(e->arg, s, env);
  return new app_expr(abs, arg);
}

/// [x->s]unit = unit
expr*
substitute_unit_expr(unit_expr* e, const subst& s, environment& env)
{
  return e;
}

/// [x->s]true = true
/// [x->s]false = false
expr*
substitute_bool_expr(bool_expr* e, const subst& s, environment& env)
{
  return e;
}

/// [x->s]if e1 then e2 else e3 = if [x->s]e1 then [x->s]e2 else [x->s]e2
expr*
substitute_cond_expr(cond_expr* e, const subst& s, environment& env)
{
  expr* cond = substitute(e->cond, s, env);
  expr* tval = substitute(e->tval, s, env);
  expr* fval = substitute(e->cond, s, env);
  return new cond_expr(cond, tval, fval);
}

/// [x->s](e1 and e2) = [x->s]e1 and [x->s]e2
expr*
substitute_and_expr(and_expr* e, const subst& s, environment& env)
{
  expr* lhs = substitute(e->lhs, s, env);
  expr* rhs = substitute(e->rhs, s, env);
  return new and_expr(lhs, rhs);
}

/// [x->s](e1 or e2) = [x->s]e1 or [x->s]e2
expr*
substitute_or_expr(or_expr* e, const subst& s, environment& env)
{
  expr* lhs = substitute(e->lhs, s, env);
  expr* rhs = substitute(e->rhs, s, env);
  return new or_expr(lhs, rhs);
}

/// [x->s](not e1) = not [x->s]e1
expr*
substitute_not_expr(not_expr* e, const subst& s, environment& env)
{
  expr* arg = substitute(e->arg, s, env);
  return new not_expr(arg);
}

/// [x->s]n = n
expr*
substitute_int_expr(int_expr* e, const subst& s, environment& env)
{
  return new int_expr(e->val);
}

/// [x->s](e1 @ e2) = [x->s]e1 @ [x->s]e2
expr*
substitute_arith_expr(arith_expr* e, const subst& s, environment& env)
{
  expr* lhs = substitute(e->lhs, s, env);
  expr* rhs = substitute(e->rhs, s, env);
  return new arith_expr(e->op, lhs, rhs);
}

/// [x->s](e1 @ e2) = [x->s]e1 @ [x->s]e2
expr*
substitute_rel_expr(rel_expr* e, const subst& s, environment& env)
{
  expr* lhs = substitute(e->lhs, s, env);
  expr* rhs = substitute(e->rhs, s, env);
  return new rel_expr(e->op, lhs, rhs);
}

/// [x->s]{e1, e2, ..., en} = {[x-s>]e1, [x->s]e2, ..., [x->s]en}
expr*
substitute_tuple_expr(tuple_expr* e, const subst& s, environment& env)
{
  expr_seq elems;
  for (expr* x : e->elems)
    elems.push_back(substitute(x, s, env));
  return new tuple_expr(std::move(elems));
}

/// [x->s](e1.n) = [x->s]e1.n
expr*
substitute_proj_expr(proj_expr* e, const subst& s, environment& env)
{
  expr* base = substitute(e->base, s, env);
  return new proj_expr(base, e->elem);
}

/// [x->s](new T e) = new T [x->s]e
///
/// FIXME: Substitute through the type? Or is that a separate form of
/// substitution?
expr*
substitute_new_expr(new_expr* e, const subst& s, environment& env)
{
  expr* init = substitute(e->init, s, env);
  return new new_expr(e->ty, init);
}

/// [x->s](delete e) = delete [x->s]e
expr*
substitute_del_expr(del_expr* e, const subst& s, environment& env)
{
  expr* obj = substitute(e->obj, s, env);
  return new del_expr(obj);
}

/// [x->s](&e) = &[x->s]e
expr*
substitute_addr_expr(addr_expr* e, const subst& s, environment& env)
{
  expr* obj = substitute(e->obj, s, env);
  return new addr_expr(obj);
}

/// [x->s](*e) = *[x->s]e
expr*
substitute_deref_expr(deref_expr* e, const subst& s, environment& env)
{
  expr* ptr = substitute(e->ptr, s, env);
  return new deref_expr(ptr);
}

/// [x->s](@e) = @[x->s]e
expr*
substitute_load_expr(load_expr* e, const subst& s, environment& env)
{
  expr* obj = substitute(e->obj, s, env);
  return new load_expr(obj);
}

/// [x->s](e1 = e2) = [x->s]e1 = [x->s]e2
expr*
substitute_store_expr(store_expr* e, const subst& s, environment& env)
{
  expr* obj = substitute(e->obj, s, env);
  expr* val = substitute(e->val, s, env);
  return new store_expr(obj, val);
}

/// [x->s]\(x1, x2, ..., xn).e = \(x1, x2, ..., xn).[x->s]e
expr*
substitute_fn_expr(fn_expr* e, const subst& s, environment& env)
{
  scope_guard g(env);
  var_seq parms;
  for (var* x : e->parms)
    parms.push_back(rename(x, env));
  expr* body = substitute(e->body, s, env);
  return new fn_expr(std::move(parms), body);
}

/// [x->s]e(e1, e2, ..., en) = [x->s]e([x->s]e1, [x->s]e2, ..., [x->s]en)
expr*
substitute_call_expr(call_expr* e, const subst& s, environment& env)
{
  expr* abs = substitute(e->abs, s, env);
  expr_seq args;
  for (expr* a : e->args)
    args.push_back(substitute(a, s, env));
  return new call_expr(abs, std::move(args));
}

/// [x->s]_ = _
expr*
substitute_ph_expr(ph_expr* e, const subst& s, environment& env)
{
  return e;
}

/// [x->s]e(:e1, e2, ..., en:) = [x->s]e(:[x->s]e1, [x->s]e2, ..., [x->s]en:)
expr*
substitute_bind_expr(bind_expr* e, const subst& s, environment& env)
{
  expr* abs = substitute(e->abs, s, env);
  expr_seq args;
  for (expr* a : e->args)
    args.push_back(substitute(a, s, env));
  return new bind_expr(abs, std::move(args));
}

/// [x->s](let x = e1 in e2) = (let x = [x->s]e1 in [x->s]e2)
///
/// The variable x is renamed in both the initializer and body of the
/// the expression. Note that x could be recursive (can we detect this?).
expr*
substitute_let_expr(let_expr* e, const subst& s, environment& env)
{
  scope_guard g(env);
  var* x = rename(e->parm, env);
  expr* init = substitute(e->init, s, env);
  expr* body = substitute(e->body, s, env);
  return new let_expr(x, init, body);  
}

/// [x->s](e1; e2) = [x->s]e1; [x->s]e2
expr*
substitute_seq_expr(seq_expr* e, const subst& s, environment& env)
{
  expr* first = substitute(e->first, s, env);
  expr* second = substitute(e->second, s, env);
  return new seq_expr(first, second);
}

/// Substitute through the expression using the given environment.
expr*
substitute(expr* e, const subst& s, environment& env)
{
  switch (e->kind) {
#define def_expr(E) \
  case ek_ ## E: \
    return substitute_ ## E ## _expr((E ##_expr*)e, s, env);
#include "expr.def"
  }
}

/// Substitute through the expression.
expr*
substitute(expr* e, const subst& s)
{
  environment env;
  return substitute(e, s, env);
}
