
#include "ast.hpp"

#include <cassert>
#include <iostream>


/// Returns the abstraction \x.x.
expr* 
make_id()
{
  environment env;
  scope_guard g(env);
  var* x = env.declare("x");
  return new abs_expr(x, new ref_expr(x));
}

/// Returns the church boolean for b: \t.\f.<b>
expr* 
make_church_bool(bool b)
{
  environment env;
  scope_guard g1(env);
  var* t = env.declare("t");
  scope_guard g2(env);
  var* f = env.declare("f");
  
  return new abs_expr(
    t, new abs_expr(
      f, new ref_expr(b ? t : f)));
}

/// Returns the logical operator and. Takes literal false.
expr*
make_and(expr* f)
{
  environment env;
  scope_guard g1(env);
  var* b = env.declare("b");
  scope_guard g2(env);
  var* c = env.declare("c");

  return new abs_expr(
    b, new abs_expr(
      c, new app_expr(
        new app_expr(new ref_expr(b), new ref_expr(c)), f)));
}

// Returns the church numeral for n.
expr*
make_church_num(int n)
{
  environment env;
  scope_guard g1(env);
  var* s = env.declare("s");
  scope_guard g2(env);
  var* z = env.declare("z");

  // Build the body inside out. The pattern is:
  //
  //    0 = \s.\z. z
  //    1 = \s.\z. s z
  //    2 = \s. \z. s (s z)
  //    3 = \s. \z. s (s (s z))
  //    ...
  //    n = s^n z
  expr* body = new ref_expr(z);
  while (n != 0) {
    body = new app_expr(new ref_expr(s), body);
    --n;
  }
  
  // Build the abstractions that bind the references.
  return new abs_expr(s, new abs_expr(z, body));
}

expr* 
test_and(expr* op, expr* e1, expr* e2)
{
  expr* e = new app_expr(new app_expr(op, e1), e2);
  while (!is_value(e)) {
    std::cout << e << '\n';
    e = reduce(e);
  }
  std::cout << e << '\n';
  return e;
}

expr* boolean(bool b) { return new bool_expr(b); }

expr* truth() { return boolean(true); }

expr* falsity() { return boolean(false); }

expr* ph() { return new ph_expr(); }

expr* ref(var* v) { return new ref_expr(v); }


int 
main()
{
  // expr* id = make_id();
  // std::cout << id << '\n';
  // subst s {
  //   {((abs_expr*)id)->parm, id}
  // };
  // expr* r = substitute(id, s);
  // std::cout << r << '\n';

#if 0
  expr* t = make_church_bool(true);
  expr* f = make_church_bool(false);
  expr* andb = make_and(f);

  std::cout << "true == " << t << '\n';
  std::cout << "false == " << f << '\n';

  // Truth table for and.
  test_and(andb, t, t);
  test_and(andb, t, f);
  test_and(andb, f, t);
  test_and(andb, f, f);

  expr* n0 = make_church_num(0);
  // expr* n1 = make_church_num(1);
  // expr* n2 = make_church_num(2);
  // expr* n3 = make_church_num(3);

  std::cout << "0 == " << n0 << '\n';

  std::cout << equal(f, n0) << '\n';
  std::cout << alpha_equivalent(f, n0) << '\n';
  std::cout << alpha_equivalent(n0, f) << '\n';

  // std::cout << "id = " << id << '\n';
  // std::cout << "true = " << t << '\n';
  // std::cout << "false = " << f << '\n';
  // std::cout << "and = " << andb << '\n';
  // std::cout << "0 = " << n0 << '\n';
  // std::cout << "1 = " << n1 << '\n';
  // std::cout << "2 = " << n2 << '\n';
  // std::cout << "3 = " << n3 << '\n';
#endif

  expr* t = truth();
  expr* f = falsity();

  {
    std::cout << "LET\n";
    environment env;
    scope_guard g1(env);
    var* x = env.declare("x");
    expr* let = new let_expr(x, 
      new bool_expr(true), 
      new cond_expr(new ref_expr(x), new bool_expr(false), new bool_expr(true)));
    std::cout << let << '\n';
    
    expr* ds = desugar(let);
    std::cout << ds << '\n';
    
    ds = reduce(ds);
    std::cout << ds << '\n';
    
    ds = reduce(ds);
    std::cout << ds << '\n';
  }

  {
    std::cout << "LAMBDA\n";
    environment env;
    scope_guard g1(env);
    var* a = env.declare("a");
    var* b = env.declare("b");
    var* c = env.declare("c");
    expr* lam = new fn_expr({a, b, c}, ref(b));
    std::cout << lam << '\n';
    std::cout << desugar(lam) << '\n';

    std::cout << "CALL\n";
    expr* call = new call_expr(lam, {t, f, t});
    std::cout << call << '\n';
    // std::cout << desugar(call) << '\n';
    std::cout << reduce(call) << '\n';

    std::cout << "BIND\n";
    expr* bind = new bind_expr(lam, {ph(), t, ph()}); 
    std::cout << bind << '\n';
    std::cout << reduce(bind) << '\n';

    std::cout << "CURRY\n";
    expr* curr = curry(lam);
    std::cout << curr << '\n';
    expr* uncurr = uncurry(curr);
    std::cout << uncurr << '\n';
  }

  {
    std::cout << "SEQ\n";
    expr* seq = new seq_expr(new bool_expr(true), new bool_expr(false));
    std::cout << seq << '\n';
    std::cout << desugar(seq) << '\n';
    std::cout << reduce(desugar(seq)) << '\n';
  }

}
