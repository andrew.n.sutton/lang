#pragma once

#include <cassert>
#include <list>
#include <queue>
#include <string>
#include <unordered_map>
#include <vector>

struct var;
struct expr;
struct type;

// -------------------------------------------------------------------------- //
// Variables

/// A variable in an abstraction.
struct var
{
  var(const std::string& n)
    : name(n)
  { }

  var(const std::string& n, type* t)
    : name(n), ty(t)
  { }
  
  std::string name; // The name of the variable.
  type* ty; // The type of the variable, possibly null.
};

/// A sequence of variables.
using var_seq = std::vector<var*>;

// -------------------------------------------------------------------------- //
// Expressions

enum expr_kind : int 
{
#define def_expr(E) ek_ ## E,
#include "expr.def"
};

// Arithmetic and bitwise operators
enum arith_op : int
{
  op_add,
  op_sub,
  op_mul,
  op_div,
  op_rem,

  op_and,
  op_ior,
  op_xor,
  op_shl,
  op_shr,
};

// Bitwise operators
enum rel_op : int
{
  op_eq,
  op_ne,
  op_lt,
  op_gt,
  op_le,
  op_ge,
};

/// Returns a string that spells the arithmetic operator.
const char* get_spelling(arith_op op);

/// Returns a string that spells the relational operator.
const char* get_spelling(rel_op op);

/// The base class of all expressions.
struct expr
{
  expr(expr_kind k)
    : kind(k), ty(nullptr)
  { }

  virtual ~expr() { }

  expr_kind kind;
  type* ty;
};

/// A sequence of expressions.
using expr_seq = std::vector<expr*>;

/// A reference to a parameter.
struct ref_expr : expr
{
  ref_expr(var* p)
    : expr(ek_ref), ref(p)
  { }

  var* ref; // The referenced parameter.
};

/// A lambda abstraction.
struct abs_expr : expr
{
  abs_expr(var* v, expr* e)
    : expr(ek_abs), parm(v), body(e)
  { }

  var* parm; // The lambda parameter
  expr* body; // The abstracted expression
};

/// An application of arguments to a lambda abstraction.
struct app_expr : expr
{
  app_expr(expr* e1, expr* e2)
    : expr(ek_app), abs(e1), arg(e2)
  { }

  expr* abs;
  expr* arg;
};

/// The unit literal.
struct unit_expr : expr
{
  unit_expr()
    : expr(ek_unit)
  { }
};

/// A boolean literal.
struct bool_expr : expr
{
  bool_expr(bool b)
    : expr(ek_bool), val(b)
  { }

  bool val;
};

/// A conditional (if) expression.
struct cond_expr : expr
{
  cond_expr(expr* e1, expr* e2, expr* e3)
    : expr(ek_cond), cond(e1), tval(e2), fval(e3)
  { }
  
  expr* cond;
  expr* tval;
  expr* fval;
};

/// The expression `e1 and e2`.
struct and_expr : expr
{
  and_expr(expr* e1, expr* e2)
    : expr(ek_and), lhs(e1), rhs(e2)
  { }
  
  expr* lhs;
  expr* rhs;
};

/// The expression `e1 or e2`.
struct or_expr : expr
{
  or_expr(expr* e1, expr* e2)
    : expr(ek_or), lhs(e1), rhs(e2)
  { }
  
  expr* lhs;
  expr* rhs;
};

/// The expression `not e1`.
struct not_expr : expr
{
  not_expr(expr* e)
    : expr(ek_not), arg(e)
  { }
  
  expr* arg;
};

/// An integer literal.
struct int_expr : expr
{
  int_expr(int n)
    : expr(ek_int), val(n)
  { }

  int val;
};

/// An operator of the form `e1 @ e2` where @ is one of the arithmetic
/// or binary operators.
struct arith_expr : expr
{
  arith_expr(arith_op op, expr* e1, expr* e2)
    : expr(ek_arith), op(op), lhs(e1), rhs(e2)
  { }

  arith_op op;
  expr* lhs;
  expr* rhs;
};

/// A relational operator of the form `e1 @ e2` where @ is one of the
/// equality or ordering operators.
struct rel_expr : expr
{
  rel_expr(rel_op op, expr* e1, expr* e2)
    : expr(ek_rel), lhs(e1), rhs(e2)
  { }

  rel_op op;
  expr* lhs;
  expr* rhs;
};

/// A tuple.
struct tuple_expr : expr
{
  tuple_expr(const expr_seq& es)
    : expr(ek_tuple), elems(es)
  { }

  tuple_expr(expr_seq&& es)
    : expr(ek_tuple), elems(std::move(es))
  { }

  expr_seq elems;
};

/// A projection on a tuple or record.
struct proj_expr : expr
{
  proj_expr(expr* e1, int n)
    : expr(ek_proj), base(e1), elem(n)
  { }

  expr* base;
  int elem;
};


/// Represents expressions of the form `new e1 as T`. This creates an object
/// in the free store whose type is deduced from `e1`.
struct new_expr : expr
{
  new_expr(type* t, expr* e)
    : expr(ek_new), ty(t), init(e)
  { }
  
  type* ty;
  expr* init;
};

/// Represents expressions of the form `delete e1`. This destroys an object
/// previously allocated in the free store.
struct del_expr : expr
{
  del_expr(expr* e)
    : expr(ek_del), obj(e)
  { }

  expr* obj;
};

/// Represents expressions of the form `&e` where `e` refers to an object.
struct addr_expr : expr
{
  addr_expr(expr* e)
    : expr(ek_addr), obj(e)
  { }

  expr* obj;
};

/// Represents expressions of the form `*e` where `e` is a pointer. This 
/// yields a reference to an object.
struct deref_expr : expr
{
  deref_expr(expr* e)
    : expr(ek_deref), ptr(e)
  { }

  expr* ptr;
};

/// Represents expressions of the form `@e` where `e` refers to an object.
/// This yields the value in the object.
///
/// \note This corresponds to an lvalue-to-rvalue conversion in C++. These
/// are normally inserted implicitly during the translation process.
struct load_expr : expr
{
  load_expr(expr* e)
    : expr(ek_load), obj(e)
  { }

  expr* obj;
};

/// Represents expressions of the form `e1 = e2` where `e1` refers to an object
/// and `e2` a value. This stores the value of `e2` in the object referred to
/// by `e1`.
struct store_expr : expr
{
  store_expr(expr* e1, expr* e2)
    : expr(ek_store), obj(e1), val(e2)
  { }

  expr* obj;
  expr* val;
};


/// A multi-parameter lambda abstraction of the form `\(x1, x2, ..., xn).e`.
/// This is syntactic sugar for '\x1.\x2. ... \xn.e.'
struct fn_expr : expr
{
  fn_expr(const var_seq& vs, expr* e)
    : expr(ek_fn), parms(vs), body(e)
  { }

  fn_expr(var_seq&& vs, expr* e)
    : expr(ek_fn), parms(std::move(vs)), body(e)
  { }

  var_seq parms;
  expr* body;
};

/// A multi-argument application of the form `e(e1, e2, ..., en)`. This is
/// syntactic sugar for `e e1 e2 ... en`.
struct call_expr : expr
{
  call_expr(expr* e, const expr_seq& es)
    : expr(ek_call), abs(e), args(es)
  { }

  call_expr(expr* e, expr_seq&& es)
    : expr(ek_call), abs(e), args(std::move(es))
  { }

  expr* abs;
  expr_seq args;
};

/// A placeholder for a value. This can be used only within the argument
/// list of a bind expression.
struct ph_expr : expr
{
  ph_expr()
    : expr(ek_ph)
  { }
};

/// A multi-argument partial application of the form `e(e1, e2, ..., en)` 
/// where zero or more ei are placeholders. In other words, the expression 
/// yields a function where non-placehoder values are bound to their 
/// parameters. If no placeholders are present, the resulting function is
/// a constant function (or generator if we had side effects).
struct bind_expr : expr
{
  bind_expr(expr* e, const expr_seq& es)
    : expr(ek_bind), abs(e), args(es)
  { }

  bind_expr(expr* e, expr_seq&& es)
    : expr(ek_bind), abs(e), args(std::move(es))
  { }

  expr* abs;
  expr_seq args;
};

/// A let expression of the form 'let x = e1 in e2'. This is syntactic sugar
/// for '(\x.e2) e1'.
struct let_expr : expr
{
  let_expr(var* x, expr* e1, expr* e2)
    : expr(ek_let), parm(x), init(e1), body(e2)
  { }

  var* parm;
  expr* init;
  expr* body;
};

/// A sequence expression of the form 'e1 ; e2'. This is syntactic sugar for
/// '(\_.e2) e1'. The parameter name is essentially ignored.
struct seq_expr : expr
{
  seq_expr(expr* e1, expr* e2)
    : expr(ek_seq), first(e1), second(e2)
  { }

  expr* first;
  expr* second;
};

// -------------------------------------------------------------------------- //
// Types

enum type_kind : int
{
#define def_type(T) tk_ ## T,
#include "type.def"
};

/// The base class of all types.
struct type
{
  type(type_kind k)
    : kind(k)
  { }

  virtual ~type() { }
  
  type_kind kind;
};

/// A sequence of types.
using type_seq = std::vector<type*>;

/// The type `Unit`, whose only value is `unit`.
struct unit_type : type
{
  unit_type()
    : type(tk_unit)
  { }
};

/// The type `Bool`, whose values `true` and `false`.
struct bool_type : type
{
  bool_type()
    : type(tk_bool)
  { }
};

/// The type `Int`, whose values are integers in the range -2^31 to 2^31 - 1.
struct int_type : type
{
  int_type()
    : type(tk_int)
  { }
};

/// The placeholder type.
struct ph_type : type
{
  ph_type() 
    : type(tk_ph)
  { }
};

/// The type `T1 -> T2` whose values are abstractions taking `T1` values
/// as inputs and yielding `T2` values as outputs.
///
/// \todo What's the type of a lambda expression?
struct arrow_type : type
{
  arrow_type(type* t1, type* t2)
    : type(tk_arrow), parm(t1), ret(t2)
  { }

  type* parm;
  type* ret;
};

/// The type `(T1, T2, ..., Tn) -> T` whose values are lambda abstractions
/// taking `T1, T2, ..., Tn` values as inputs and yielding `T` values as
/// outputs.
struct fn_type : type
{
  fn_type(const type_seq& ts, type* t)
    : type(tk_fn), parms(ts), ret(t)
  { }

  fn_type(type_seq&& ts, type* t)
    : type(tk_fn), parms(std::move(ts)), ret(t)
  { }

  type_seq parms;
  type* ret;
};

/// The type `{T1, T2, ..., Tn}` whose values are tuples.
struct tuple_type : type
{
  tuple_type(const type_seq& ts)
    : type(tk_tuple), elems(ts)
  { }

  tuple_type(type_seq&& ts)
    : type(tk_tuple), elems(std::move(ts))
  { }

  type_seq elems;
};

/// A field is a labeled (i.e., named) member of a record or variant,
/// spelled `l:T``.
struct field
{
  std::string id;
  type* ty;
};

/// A sequence of fields.
using field_seq = std::vector<field*>;

/// The type `record {f1, f2, ..., fn}`.
///
/// \todo Record types are currently typed structurally. It might be nice
/// to also support nominally typed records (i.e., as with C++). 
///
/// \todo When defining subtyping, be sure that order matters; you can't
/// convert between objects through a binary interface if the order of members
/// is allowed to change.
struct record_type : type
{
  record_type(const field_seq& fs)
    : type(tk_record), fields(fs)
  { }

  record_type(field_seq&& fs)
    : type(tk_record), fields(fs)
  { }

  field_seq fields;
};

/// The type `variant {f1, f2, ..., fn}`.
///
/// \todo Variant types are currently typed structurally. It might be nice
/// to also support nominally typed records (i.e., as with C++).
struct variant_type : type
{
  variant_type(const field_seq& fs)
    : type(tk_variant), fields(fs)
  { }

  variant_type(field_seq&& fs)
    : type(tk_variant), fields(fs)
  { }

  field_seq fields;
};

/// Represents pointer types which have the form `T*`. Pointer types represent
/// the set of locations of objects of type `T`.
///
/// Pointer types are object types, meaning that they can be stored.
struct ptr_type : type
{
  ptr_type(type* t)
    : type(tk_ptr), elem(t)
  { }

  type* elem;
};


/// Represents references types which have the form `T&`. Reference types
/// represent the set of locations of objects of type `T`.
///
/// Unlike pointer types, reference types are not object types. Declaring
/// a reference does not (necessarily) introduce additional storage.
struct ref_type : type
{
  ref_type(type* t)
    : type(tk_ref), obj(t)
  { }

  type* obj;
};

// -------------------------------------------------------------------------- //
// Queries

/// Returns true if `e` is a value (i.e., in normal form).
bool is_value(const expr* e);

/// Returns true if `e` is a lambda abstraction (of any kind).
bool is_lambda(const expr* e);

/// Returns true if `t` is an object type.
bool is_object_type(const type* t);

/// Returns true if `t` is a pointer type.
bool is_pointer_type(const type* t);

/// Returns true if `t` is a reference type.
bool is_reference_type(const type* t);

/// Returns true when the expressions e1 and e2 have the same syntax.
bool equal(const expr* e1, const expr* e2);

/// Return true when the types t1 and t2 are the same.
bool equal(const type* t1, const type* t2);

/// Returns true when the expressions e1 and e2 are alpha equivalent. Two
/// expressions are alpha equivalent when they have the same syntax except
/// for the names of variables (i.e., when one is a renaming of the other).
bool alpha_equivalent(const expr* e1, const expr* e2);

/// Returns true when the types t1 and t2 are alpha equivalent. 
bool alpha_equivalent(const expr* e1, const expr* e2);

/// Returns true when e1 and e2 are beta equivalent.
bool beta_equivalent(const expr* e1, const expr* e2);

// -------------------------------------------------------------------------- //
// Printing

/// Defined only in dump.cpp.
struct dumper;

/// Dump the expression to std::cerr.
void dump(const expr* e);

/// Dump the expression to the given output stream.
void dump(dumper& os, const expr* e);

/// Print the expression `e` to `os`.
void print(std::ostream& os, const expr* e);

/// Print the type `t` to `os`.
void print(std::ostream& os, const type* t);

/// Write the expression to the output stream.
std::ostream& operator<<(std::ostream& os, const expr* e);
std::ostream& operator<<(std::ostream& os, const type* t);

// -------------------------------------------------------------------------- //
// Scope

/// A scope contains the list of variables that are visible within a region 
/// of code. This is more general than needed for untyped lambda calculus
/// in that it allows multiple variables at any particular scope.
struct scope : std::unordered_map<std::string, var*>
{
  /// Declares a new variable.
  var* declare(const std::string& n);
  
  /// Declares a variable with type t.
  var* declare(const std::string& n, type* t);

  /// Returns the corresponding variable for s.
  var* lookup(const std::string& n);
};

/// The environment maintains information about a translation for the purpose
/// of name binding and lookup.
struct environment : std::vector<scope>
{
  // Enter a new scope.
  void enter_scope() { emplace_back(); }

  // Leave the current scope.
  void leave_scope() { pop_back(); }

  // Declares a new variable with the name s.
  var* declare(const std::string& n);

  // Declares a new variable with the name s with type t.
  var* declare(const std::string& n, type* t);

  // Returns the innermost declaration of s.
  var* lookup(const std::string& n);
};

// A helper class that maintains scope.
struct scope_guard
{
  scope_guard(environment& env) 
    : env(env) 
  { 
    env.enter_scope(); 
  }
  
  ~scope_guard() 
  { 
    env.leave_scope(); 
  }
  
  environment& env;
};

// -------------------------------------------------------------------------- //
// Typing

/// Returns the type of the expression `e`. Throws an exception if the type
/// cannot be computed. The computed type is cached in the expression.
type* typecheck(expr* e);

// -------------------------------------------------------------------------- //
// Desugaring

/// Returns the desugared form of an expression.
expr* desugar(expr* e);

// -------------------------------------------------------------------------- //
// Substitution

/// A substitution record is a set of substitutions. Each substitution
/// replaces a bound variable with an expression.
struct subst : std::unordered_map<var*, expr*>
{
  using std::unordered_map<var*, expr*>::unordered_map;
};

/// Substitute s through the expression e.
expr* substitute(expr* e, const subst& sub);

/// Substitute s through the expression e, given the environment env.
expr* substitute(expr* e, const subst& sub, environment& env);

// -------------------------------------------------------------------------- //
// Currying

/// Transform a lambda expression into a sequence of abstractions.
expr* curry(expr* e);

/// Transform a sequence of abstractions into a lambda expression.
expr* uncurry(expr* e);

// -------------------------------------------------------------------------- //
// Reduction (rewriting)

/// Perform a single-step reduction on e.
expr* reduce(expr* e);

// -------------------------------------------------------------------------- //
// Evaluation

struct location;
struct value;

enum store_kind
{
  sk_null,
  sk_static,
  sk_auto,
  sk_free,
};

/// Represents the location of an object in the static store.
struct static_location
{
  static_location(int n)
    : index(n)
  { }

  int index;
};

/// Represents the location of an object on the stack.
struct auto_location
{
  auto_location(int f, int n)
    : frame(f), index(n)
  { }

  int frame;
  int index;
};

/// Represents the location of an object in the free store.
struct free_location
{
  free_location(int n)
    : index(n)
  { }

  int index;
};

bool operator==(const static_location& a, const static_location& b);
bool operator!=(const static_location& a, const static_location& b);

bool operator==(const auto_location& a, const auto_location& b);
bool operator!=(const auto_location& a, const auto_location& b);

bool operator==(const free_location& a, const free_location& b);
bool operator!=(const free_location& a, const free_location& b);


/// Represents an abstract location of an object or sub-object in a store.
/// The location value explicitly inc.
///
/// FIXME: Encode an optional path to a sub-object.
struct location
{
  location()
    : kind(sk_null)
  { }

  location(static_location loc)
    : kind(sk_static), sl(loc)
  { }
  
  location(auto_location loc)
    : kind(sk_auto), al(loc)
  { }

  location(free_location loc)
    : kind(sk_free), fl(loc)
  { }

  /// Converts to true if the location is non-null.
  explicit operator bool() const { return kind != sk_null; }

  /// The store in which the object is found.
  store_kind kind;

  /// The location of the complete object.
  union {
    static_location sl;
    auto_location al;
    free_location fl;
  };

  /// The path to a sub-object. Can be empty.
  std::vector<int> path;
};

bool operator==(const location& a, const location& b);
bool operator!=(const location& a, const location& b);

/// Kinds of values.
enum value_kind
{
  vk_undef,
  vk_unit,
  vk_int,
  vk_abs,
  vk_fn,
  vk_agg,
  vk_loc,
};

/// The value is undefined or indeterminate. This is primarily used to
/// represent the uninitialized state of objects.
enum undef_value { undef_v };

/// The unit value.
enum unit_value { unit_v };

/// An integer value.
using int_value = int;

/// An expression value.
using expr_value = const expr*;

/// An aggregate value is an array of values. The "shape" of sub-objects
/// need not be the same. This can be used to store both arrays and
/// tuples.
using agg_value = std::vector<value>;

/// A location value.
using loc_value = location;

/// The value representation is a storage buffer large enough to hold
/// any aggregate values.
union value_rep
{
  value_rep() : undef() { }
  value_rep(undef_value v) : undef(v) { }
  value_rep(unit_value v) : unit(v) { }
  value_rep(int_value v) : num(v) { }
  value_rep(expr_value v) : ex(v) { }
  value_rep(const agg_value& v) : agg(v) { }
  value_rep(agg_value&& v) : agg(std::move(v)) { }
  value_rep(const loc_value& v) : loc(v) { }
  value_rep(loc_value&& v) : loc(std::move(v)) { }
  ~value_rep() { }
  
  undef_value undef;
  unit_value unit;
  int_value num;
  expr_value ex;
  agg_value agg;
  loc_value loc;
};

/// A value computed by an expression. This is the disjoint union of all
/// "shapes" of values produced by expressions of different types.
struct value
{
  /// Constructs an undefined value.
  value() : kind(vk_undef), rep() { }

  /// Explicitly constructs an undefined value.
  explicit value(undef_value v) 
    : kind(vk_undef), rep(v) 
  { }
  
  /// Constructs a unit value.
  explicit value(unit_value u)
    : kind(vk_unit), rep(u)
  { }
  
  /// Constructs an integer value.
  explicit value(bool b)
    : kind(vk_int), rep((int)b)
  { }
  
  /// Constructs an integer value.
  explicit value(int n)
    : kind(vk_int), rep(n)
  { }
  
  /// Constructs an abstraction value.
  explicit value(const abs_expr* e)
    : kind(vk_abs), rep(e)
  { }
  
  /// Constructs a function value.
  explicit value(const fn_expr* e)
    : kind(vk_fn), rep(e)
  { }
  
  /// Constructs an aggregate value.
  explicit value(const agg_value& v)
    : kind(vk_agg), rep(v)
  { }
  
  /// Constructs an aggregate value.
  explicit value(agg_value&& v)
    : kind(vk_agg), rep(std::move(v))
  { }

  /// Constructs an location value.
  explicit value(const loc_value& v)
    : kind(vk_loc), rep(v)
  { }
  
  /// Constructs an location value.
  explicit value(loc_value&& v)
    : kind(vk_loc), rep(std::move(v))
  { }

  value(const value& v);
  value(value&& v);
  ~value();

  value& operator=(const value& v);
  value& operator=(value&& v);

  value_kind kind;
  value_rep rep;
};

// Equality comparison
bool operator==(const value& a, const value& b);
bool operator!=(const value& a, const value& b);

/// A sequence of values.
using value_seq = std::vector<value>;

/// The static store provides storage and bindings for global variables.
struct static_store
{
  /// Allocate storage for a variable in this frame.
  location allocate(const var* x);

  /// Returns the location of the variable x or nullptr.
  location lookup(const var* x);

  /// The set of locally allocated objects.
  std::vector<value> objs;

  /// Stored mappings from variables to their locations.
  std::unordered_map<const var*, location> locs;
};

/// A (call) frame is a binding of variables to their locations.
///
/// Note that this data structure also supports an 'alloca' operation, which
/// can create unnamed local objects.
///
/// FIXME: We could avoid implementing the lookup function by storing caching
/// the offset of each parameter in the parameter list. We could then generate
/// a location for a variable as: (current frame depth, index). There are 
/// likely some unresolved issues with lambda capture that might make this
/// a bit difficult.
struct frame
{
  frame(int n)
    : depth(n)
  { }

  /// Allocate an unnamed local object in this frame.
  location allocate();

  /// Allocate storage for a variable in this frame.
  location allocate(const var* x);

  /// Returns the location of the variable x or nullptr.
  location lookup(const var* x);

  /// The depth level of the stack.
  int depth;

  /// The set of locally allocated objects.
  std::vector<value> objs;

  /// Stored mappings from variables to their locations.
  std::unordered_map<const var*, location> locs;
};

/// The automatic store is a memory store structured as a stack of frames. 
struct automatic_store : std::vector<frame>
{
  /// Enter a new stack frame.
  void enter_frame() { emplace_back(size()); }

  /// Leave a stack frame.
  void leave_frame() { pop_back(); }

  /// Allocates an unnamed local in the current frame.
  location allocate();

  /// Allocates an unnamed local in the current frame.
  location allocate(const var* x);

  /// Returns the location for the most recent binding of x.
  location lookup(const var* x);
};

/// The free store is a memory store that allows objects to be explicitly
/// allocated and deallocated.
struct free_store
{
  /// Allocate storage for an object.
  location allocate();

  /// Deallocate the given object.
  void deallocate(location);

  /// Allocated objects.
  std::vector<value> objs;

  /// A free list.
  std::priority_queue<int> free;
};

/// The runtime store is a collection of memory regions.
///
/// \todo Add a static store, a free store, and maybe even a managed store.
struct store
{
  /// Returns a reference to a stored value (i.e., object).
  value& resolve(location);

  /// The static store (i.e., global variables).
  static_store globals;

  /// The automatic store (i.e., local variable on the call stack).
  automatic_store stack;
  
  /// The free store (i.e., for new and delete).
  free_store free;
};

/// An RAII helper to manage stack frames.
struct frame_guard
{
  frame_guard(store& s)
    : sto(s)
  {
    sto.stack.enter_frame();
  }

  ~frame_guard()
  {
    sto.stack.leave_frame();
  }

  store& sto;
};

// Evaluate the expression e.
value evaluate(const expr* e);

// -------------------------------------------------------------------------- //
// Code generation

/// Compiles an expression as a module.
///
/// FIXME: This should return something.
void compile(const expr* e);
