
#include "ast.hpp"

#include <llvm/IR/LLVMContext.h>
#include <llvm/IR/Module.h>
#include <llvm/IR/Function.h>
#include <llvm/IR/Type.h>
#include <llvm/IR/IRBuilder.h>

#include <cassert>
#include <iostream>
#include <sstream>

namespace cg
{
  using variable_map = std::unordered_map<const var*, llvm::Value*>;

  enum linkage_kind
  {
    lk_internal,
    lk_external,
  };

  /// A name with an explicit linkage.
  struct link_name
  {
    link_name(linkage_kind lk, const std::string& n)
      : link(lk), name(n)
    { }
    
    linkage_kind link;
    std::string name;
  };

  /// Maintains the root codegen context.
  struct context
  {
    context();
    ~context();

    /// Generate the corresponding type.
    llvm::Type* gen_type(const type* t);
    llvm::Type* gen_unit_type(const unit_type* t);
    llvm::Type* gen_bool_type(const bool_type* t);
    llvm::Type* gen_int_type(const int_type* t);
    llvm::Type* gen_arrow_type(const arrow_type* t);
    llvm::Type* gen_fn_type(const fn_type* t);

    /// Generate an object type (e.g., function types become pointers).
    llvm::Type* gen_object_type(const type* t);
    
    llvm::LLVMContext* cxt;
  };

  /// Provides the codegen context for top-level declarations.
  struct module
  {
    module(context& cxt, const char* n);

    /// Returns the llvm context.
    llvm::LLVMContext* get_context() const { return parent->cxt; }

    /// Returns a new externally linked name.
    link_name make_external_name(const std::string& n);

    /// Returns a unique name with prefix `s` for a function with 
    /// internal linkage. 
    link_name make_unique_name(const std::string& s);

    /// Declare a new global value.
    void declare(const var* x, llvm::GlobalValue* v);

    /// Lookup a value.
    llvm::GlobalValue* lookup(const var* x) const;

    /// Generate a corresponding type.
    llvm::Type* gen_type(const type* t);

    /// Process expressions as top-level declarations.
    ///
    /// FIXME: These have the wrong name.
    void gen_expr(const expr* e);
    const expr* gen_let_expr(const let_expr* e);

    /// Generate a function from an expression.
    void gen_fn(var* x, const fn_expr* e);

    /// Generate a variable.
    void gen_var(var* x, const expr* e);

    context* parent;
    llvm::Module* mod; // The module.
    variable_map globals; // Global values.
    int count; // The unique name index.
  };

  /// Provides the codegen context for expressions.
  struct function
  {
    function(module& m, const link_name& n, const type* t, const var_seq& parms);

    /// Returns the LLVM context.
    llvm::LLVMContext* get_context() const { return parent->get_context(); }

    /// Returns the owning module.
    llvm::Module* get_module() const { return parent->mod; }

    /// Declare a new local value.
    void declare(const var* x, llvm::Value* v);

    /// Lookup a value. This may return a global value.
    llvm::Value* lookup(const var* x) const;

    /// Generate a corresponding type.
    llvm::Type* gen_type(const type* t);

    /// Generate the function definition.
    void gen_def(const expr* e);

    /// Generate a list of instructions to compute the value of e.
    llvm::Value* gen_expr(const expr* e);
    llvm::Value* gen_ref_expr(const ref_expr* e);
    llvm::Value* gen_abs_expr(const abs_expr* e);
    llvm::Value* gen_app_expr(const app_expr* e);
    llvm::Value* gen_unit_expr(const unit_expr* e);
    llvm::Value* gen_bool_expr(const bool_expr* e);
    llvm::Value* gen_cond_expr(const cond_expr* e);
    llvm::Value* gen_and_expr(const and_expr* e);
    llvm::Value* gen_or_expr(const or_expr* e);
    llvm::Value* gen_not_expr(const not_expr* e);
    llvm::Value* gen_int_expr(const int_expr* e);
    llvm::Value* gen_arith_expr(const arith_expr* e);
    llvm::Value* gen_rel_expr(const rel_expr* e);
    llvm::Value* gen_fn_expr(const fn_expr* e);
    llvm::Value* gen_call_expr(const call_expr* e);
    llvm::Value* gen_let_expr(const let_expr* e);
    llvm::Value* gen_seq_expr(const seq_expr* e);

    module* parent;
    llvm::Function* fn; // The owning function.
    llvm::BasicBlock* bb; // The current basic block.
    variable_map locals; // Local variables.
  };

// -------------------------------------------------------------------------- //
// Root context

  context::context()
    : cxt(new llvm::LLVMContext())
  { }

  context::~context() { delete cxt; }

  /// Generate the corresponding type.
  llvm::Type* 
  context::gen_type(const type* t)
  {
    switch (t->kind) {
    case tk_unit:
      return gen_unit_type((const unit_type*)t);
    case tk_bool:
      return gen_bool_type((const bool_type*)t);
    case tk_int:
      return gen_int_type((const int_type*)t);
    case tk_arrow:
      return gen_arrow_type((const arrow_type*)t);
    case tk_fn:
      return gen_fn_type((const fn_type*)t);
    default:
      throw std::logic_error("invalid type");
    }
  }

  /// The unit type is a 1 bit integer type. The value is always 0.
  ///
  /// FIXME: Can we make the unit type LLVM's void? Probably not as we
  /// could end up having void multiple void parameters.
  llvm::Type*
  context::gen_unit_type(const unit_type* t)
  {
    return llvm::IntegerType::get(*cxt, 1);
  }

  llvm::Type*
  context::gen_bool_type(const bool_type* t)
  {
    return llvm::IntegerType::get(*cxt, 1);
  }

  llvm::Type*
  context::gen_int_type(const int_type* t)
  {
    return llvm::IntegerType::get(*cxt, 32);
  }

  /// Generate the type as a pointer. The actual function type can be as needed.
  llvm::Type* 
  context::gen_arrow_type(const arrow_type* t)
  {
    llvm::Type* parm = gen_type(t->parm);
    llvm::Type* ret = gen_type(t->ret);
    llvm::Type* base = llvm::FunctionType::get(ret, {parm}, false);
    return base->getPointerTo();
  }

  /// Generate the type as a pointer. The actual function type can be as needed.
  llvm::Type* 
  context::gen_fn_type(const fn_type* t)
  {
    std::vector<llvm::Type*> parms(t->parms.size());
    std::transform(t->parms.begin(), t->parms.end(), parms.begin(), [this](const type* p) {
      return gen_type(p);
    });
    llvm::Type* ret = gen_type(t->ret);
    llvm::Type* base = llvm::FunctionType::get(ret, parms, false);
    return base->getPointerTo();
  }


// -------------------------------------------------------------------------- //
// Module context

  module::module(context& cxt, const char* n)
    : parent(&cxt), mod(new llvm::Module(n, *get_context())), count()
  { }

  link_name
  module::make_external_name(const std::string& n)
  {
    return {lk_external, n};
  }

  link_name
  module::make_unique_name(const std::string& s)
  {
    std::stringstream ss;
    ss << "__" << s << '_' << count++;
    return {lk_internal, ss.str()};
  }

  void
  module::declare(const var* x, llvm::GlobalValue* v)
  {
    assert(globals.count(x) == 0);
    globals.emplace(x, v);
  }

  llvm::GlobalValue*
  module::lookup(const var* x) const
  {
    auto iter = globals.find(x);
    if (iter != globals.end())
      return llvm::cast<llvm::GlobalValue>(iter->second);
    else
      return nullptr;
  }

  /// Generate a corresponding type. Types are computed and cached globally.
  llvm::Type*
  module::gen_type(const type* t)
  {
    return parent->gen_type(t);
  }

  /// Process expressions as top-level declarations. This processes each
  /// top-level expression iteratively. Each subroutine returns the next
  /// expression to evaluate.
  void 
  module::gen_expr(const expr* e)
  {
    while (e) {
      switch (e->kind) {
      case ek_let:
        e = gen_let_expr((const let_expr*)e);
        break;
      case ek_unit:
        e = nullptr;
        break;
      default:
        throw std::logic_error("invalid top-level expression");      
      }
    }
  }

  // Based on the expression kind of the initializer, generate a top-level
  // declaration.
  const expr* 
  module::gen_let_expr(const let_expr* e)
  {
    const expr* init = e->init;
    switch (init->kind) {
    case ek_abs:
      // Abstractions must be lowered into function expressions.
      throw std::logic_error("invalid top-level binding");
    case ek_fn:
      gen_fn(e->parm, (const fn_expr*)init);
      break;
    default:
      gen_var(e->parm, init);
      break;
    }
    return e->body;
  }

  /// Generate a function from the fn expression.
  void 
  module::gen_fn(var* x, const fn_expr* e)
  {
    function fn(*this, make_external_name(x->name), e->ty, e->parms);
    fn.gen_def(e->body);
  }

  /// Generate a variable.
  ///
  /// FIXME: To declare a module-level variable we need to:
  ///
  ///   - determine if it has storage (not yet)
  ///   - compute the initializer (if possible)
  ///   - or generate an dynamic initializer and register a constructor.
  ///
  /// Clearly, this is not straightforward.
  void 
  module::gen_var(var* x, const expr* e)
  {

  }

// -------------------------------------------------------------------------- //
// Function context

  static llvm::Function::LinkageTypes
  get_linkage(const link_name& n)
  {
    return n.link == lk_external ? llvm::Function::ExternalLinkage : llvm::Function::InternalLinkage;
  }

  function::function(module& m, const link_name& n, const type* t, const var_seq& parms)
    : parent(&m), fn(), bb()
  {
    // Build the function.
    llvm::Type* ty = gen_type(t)->getPointerElementType();
    llvm::Function* fn = llvm::Function::Create(llvm::cast<llvm::FunctionType>(ty), 
                                                get_linkage(n),
                                                n.name, 
                                                get_module());
    
    // Create and configure the parameters.
    assert(parms.size() == fn->arg_size());
    auto ai = fn->arg_begin();
    auto pi = parms.begin();
    while (ai != fn->arg_end()) {
      llvm::Argument& arg = *ai;
      const var* parm = *pi;

      // Configure each parameter.
      //
      // TODO: What other properties should we set.
      arg.setName(parm->name);
      declare(parm, &arg);
      
      ++ai;
      ++pi;
    }

    // Build the entry block.
    bb = llvm::BasicBlock::Create(*get_context(), "entry", fn);
  }


  void
  function::declare(const var* x, llvm::Value* v)
  {
    assert(locals.count(x) == 0);
    locals.emplace(x, v);
  }

  llvm::Value*
  function::lookup(const var* x) const
  {
    auto iter = locals.find(x);
    if (iter != locals.end())
      return iter->second;
    else
      return parent->lookup(x);
  }

  llvm::Type*
  function::gen_type(const type* t)
  {
    return parent->gen_type(t);
  }

  /// Creates a return instruction for the expression.
  void
  function::gen_def(const expr* e)
  {
    llvm::IRBuilder<> ir(bb);
    ir.CreateRet(gen_expr(e));
  }

  llvm::Value*
  function::gen_expr(const expr* e)
  {
    switch (e->kind) {
    case ek_ref:
      return gen_ref_expr((const ref_expr*)e);
    case ek_unit:
      return gen_unit_expr((const unit_expr*)e);
    case ek_bool:
      return gen_bool_expr((const bool_expr*)e);
    case ek_cond:
      return gen_cond_expr((const cond_expr*)e);
    case ek_and:
      return gen_and_expr((const and_expr*)e);
    case ek_or:
      return gen_or_expr((const or_expr*)e);
    case ek_not:
      return gen_not_expr((const not_expr*)e);
    case ek_int:
      return gen_int_expr((const int_expr*)e);
    case ek_arith:
      return gen_arith_expr((const arith_expr*)e);
    case ek_rel:
      return gen_rel_expr((const rel_expr*)e);
    case ek_fn:
      return gen_fn_expr((const fn_expr*)e);
    case ek_call:
      return gen_call_expr((const call_expr*)e);
    case ek_let:
      return gen_let_expr((const let_expr*)e);
    case ek_seq:
      return gen_seq_expr((const seq_expr*)e);

    case ek_abs:
    case ek_app:
    case ek_ph:
    case ek_bind:
      // These expressions should not be passed to code generation.
      throw std::logic_error("invalid expression");
    }
  }

  llvm::Value*
  function::gen_ref_expr(const ref_expr* e)
  {
    llvm::Value* ref = lookup(e->ref);
    assert(ref);
    return ref;
  }

  llvm::Value*
  function::gen_unit_expr(const unit_expr* e)
  {
    llvm::IRBuilder<> ir(bb);
    return ir.getInt1(0);
  }

  llvm::Value*
  function::gen_bool_expr(const bool_expr* e)
  {
    llvm::IRBuilder<> ir(bb);
    return ir.getInt1(e->val);
  }

  llvm::Value*
  function::gen_cond_expr(const cond_expr* e)
  {
    throw std::logic_error("not implemented");
  }

  llvm::Value*
  function::gen_and_expr(const and_expr* e)
  {
    throw std::logic_error("not implemented");
  }

  llvm::Value*
  function::gen_or_expr(const or_expr* e)
  {
    throw std::logic_error("not implemented");
  }

  llvm::Value*
  function::gen_not_expr(const not_expr* e)
  {
    throw std::logic_error("not implemented");
  }

  llvm::Value*
  function::gen_int_expr(const int_expr* e)
  {
    llvm::IRBuilder<> ir(bb);
    return ir.getInt32(e->val);
  }

  llvm::Value*
  function::gen_arith_expr(const arith_expr* e)
  {
    llvm::Value* lhs = gen_expr(e->lhs);
    llvm::Value* rhs = gen_expr(e->rhs);
    llvm::IRBuilder<> ir(bb);
    switch (e->op) {
    case op_add:
      return ir.CreateNSWAdd(lhs, rhs);
    case op_sub:
      return ir.CreateNSWSub(lhs, rhs);
    case op_mul:
      return ir.CreateNSWMul(lhs, rhs);
    case op_div:
      return ir.CreateSDiv(lhs, rhs);
    case op_rem:
      return ir.CreateSRem(lhs, rhs);
    case op_and:
      return ir.CreateAnd(lhs, rhs);
    case op_ior:
      return ir.CreateOr(lhs, rhs);
    case op_xor:
      return ir.CreateXor(lhs, rhs);
    case op_shl:
      return ir.CreateShl(lhs, rhs);
    case op_shr:
      return ir.CreateAShr(lhs, rhs);
    }
  }

  llvm::Value*
  function::gen_rel_expr(const rel_expr* e)
  {
    llvm::Value* lhs = gen_expr(e->lhs);
    llvm::Value* rhs = gen_expr(e->rhs);
    llvm::IRBuilder<> ir(bb);
    switch (e->op) {
    case op_eq:
      return ir.CreateICmpEQ(lhs, rhs);
    case op_ne:
      return ir.CreateICmpNE(lhs, rhs);
    case op_lt:
      return ir.CreateICmpSLT(lhs, rhs);
    case op_gt:
      return ir.CreateICmpSGT(lhs, rhs);
    case op_le:
      return ir.CreateICmpSLE(lhs, rhs);
    case op_ge:
      return ir.CreateICmpSGE(lhs, rhs);
    }
  }

  /// Lift the function out of the expressions to produce a new global.
  /// Returns the address of the generated function.
  ///
  /// FIXME: This doesn't handle closures. This also begs the question... what
  /// do we do if `e` needs to be closed over its free variables. Should that
  /// closure conversion be handled separately (i.e., in the type system?).
  /// Probably.
  llvm::Value*
  function::gen_fn_expr(const fn_expr* e)
  {
    function fn(*parent, parent->make_unique_name("fn"), e->ty, e->parms);
    fn.gen_def(e->body);
    return fn.fn;
  }

  llvm::Value*
  function::gen_call_expr(const call_expr* e)
  {
    throw std::logic_error("not implemented");
  }

  llvm::Value*
  function::gen_let_expr(const let_expr* e)
  {
    throw std::logic_error("not implemented");
  }

  llvm::Value*
  function::gen_seq_expr(const seq_expr* e)
  {
    throw std::logic_error("not implemented");
  }

} // namespace


void
compile(const expr* e)
{
  cg::context cxt;
  cg::module cgm(cxt, "a.lc");
  cgm.gen_expr(e);

  // WTF! What happened to my build that LLVM doesn't support this?
  // cgm.mod->dump();
}
