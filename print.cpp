#include "ast.hpp"

#include <iostream>

// FIXME: Define precedence so we can add parens intelligently.

enum enclosure
{
  parens,
  braces,
  brackets,
  angles,
};

char
open(enclosure c)
{
  switch (c) {
  case parens: return '(';
  case braces: return '{';
  case brackets: return '[';
  case angles: return '<';
  }
}

char
close(enclosure c)
{
  switch (c) {
  case parens: return ')';
  case braces: return '}';
  case brackets: return ']';
  case angles: return '>';
  }
}

struct enclose
{
  enclose(std::ostream& os, enclosure c)
    : os(os), enc(c)
  {
    os << open(enc);
  }
  
  ~enclose()
  {
    os << close(enc);
  }

  std::ostream& os;
  enclosure enc;
};


void
print(std::ostream& os, const var* v)
{
  os << v->name;
}

void
print(std::ostream& os, const var_seq& vs)
{
  for (auto iter = vs.begin(); iter != vs.end(); ++iter) {
    print(os, *iter);
    if (std::next(iter) != vs.end())
      os << ',' << ' ';
  }
}

// -------------------------------------------------------------------------- //
// Expressions

void
print(std::ostream& os, const expr_seq& es)
{
  for (auto iter = es.begin(); iter != es.end(); ++iter) {
    print(os, *iter);
    if (std::next(iter) != es.end())
      os << ',' << ' ';
  }
}

void
print_ref_expr(std::ostream& os, const ref_expr* e)
{
  print(os, e->ref);
}

void
print_abs_expr(std::ostream& os, const abs_expr* e)
{
  enclose enc(os, parens);
  os << '\\';
  print(os, e->parm);
  os << '.';
  print(os, e->body);
}

void
print_app_expr(std::ostream& os, const app_expr* e)
{
  enclose enc(os, parens);
  print(os, e->abs);
  os << ' ';
  print(os, e->arg);
}

void
print_unit_expr(std::ostream& os, const unit_expr* e)
{
  os << "unit";
}

void
print_bool_expr(std::ostream& os, const bool_expr* e)
{
  if (e->val)
    os << "true";
  else
    os << "false";
}

void
print_cond_expr(std::ostream& os, const cond_expr* e)
{
  enclose enc(os, parens);
  os << "if" << ' ';
  print(os, e->cond);
  os << ' ' << "then" << ' ';
  print(os, e->tval);
  os << ' ' << "else" << ' ';
  print(os, e->fval);
}

void
print_and_expr(std::ostream& os, const and_expr* e)
{
  enclose enc(os, parens);
  print(os, e->lhs);
  os << " && ";
  print(os, e->rhs);
}

void
print_or_expr(std::ostream& os, const or_expr* e)
{
  enclose enc(os, parens);
  print(os, e->lhs);
  os << " || ";
  print(os, e->rhs);
}

void
print_not_expr(std::ostream& os, const not_expr* e)
{
  enclose enc(os, parens);
  os << '!';
  print(os, e->arg);
}

void
print_int_expr(std::ostream& os, const int_expr* e)
{
  os << e->val;
}

void
print_arith_expr(std::ostream& os, const arith_expr* e)
{
  enclose enc(os, parens);
  print(os, e->lhs);
  os << ' ' << get_spelling(e->op) << ' ';
  print(os, e->rhs);
}

void
print_rel_expr(std::ostream& os, const rel_expr* e)
{
  enclose enc(os, parens);
  print(os, e->lhs);
  os << ' ' << get_spelling(e->op) << ' ';
  print(os, e->rhs);
}

void
print_tuple_expr(std::ostream& os, const tuple_expr* e)
{
  enclose enc(os, braces);
  print(os, e->elems);
}

void
print_proj_expr(std::ostream& os, const proj_expr* e)
{
  enclose enc(os, parens);
  print(os, e->base);
  os << '.' << e->elem;
}

void
print_new_expr(std::ostream& os, const new_expr* e)
{
  enclose enc(os, parens);
  os << "new ";
  print(os, e->ty);
  os << ' ';
  print(os, e->init);
}

void
print_del_expr(std::ostream& os, const del_expr* e)
{
  enclose enc(os, parens);
  os << "delete ";
  print(os, e->obj);
}

void
print_addr_expr(std::ostream& os, const addr_expr* e)
{
  enclose enc(os, parens);
  os << '&';
  print(os, e->obj);
}

void
print_deref_expr(std::ostream& os, const deref_expr* e)
{
  enclose enc(os, parens);
  os << '*';
  print(os, e->ptr);
}

void
print_load_expr(std::ostream& os, const load_expr* e)
{
  enclose enc(os, parens);
  os << '&';
  print(os, e->obj);
}

void
print_store_expr(std::ostream& os, const store_expr* e)
{
  enclose enc(os, parens);
  print(os, e->obj);
  os << " = ";
  print(os, e->val);
}

void
print_fn_expr(std::ostream& os, const fn_expr* e)
{
  enclose enc(os, parens);
  os << '\\';
  {
    enclose parms(os, parens);
    print(os, e->parms);
  }
  os << '.';
  print(os, e->body);
}

void
print_call_expr(std::ostream& os, const call_expr* e)
{
  enclose enc(os, parens);
  print(os, e->abs);
  {
    enclose args(os, parens);
    print(os, e->args);
  }
}

void
print_ph_expr(std::ostream& os, const ph_expr* e)
{
  os << '_';
}

void
print_bind_expr(std::ostream& os, const bind_expr* e)
{
  enclose enc(os, parens);
  print(os, e->abs);
  {
    enclose args(os, parens);
    print(os, e->args);
  }
}

void
print_let_expr(std::ostream& os, const let_expr* e)
{
  enclose enc(os, parens);
  os << "let" << ' ';
  print(os, e->parm);
  os << ' ' << '=' << ' ';
  print(os, e->init);
  os << ' ' << "in" << ' ';
  print(os, e->body);
}

void
print_seq_expr(std::ostream& os, const seq_expr* e)
{
  enclose enc(os, parens);
  print(os, e->first);
  os << " ; ";
  print(os, e->second);
}

void
print(std::ostream& os, const expr* e)
{
  switch (e->kind) {
#define def_expr(E) \
  case ek_ ## E: \
    return print_ ## E ##_expr(os, (const E ## _expr*)e);
#include "expr.def"
  }
}

// -------------------------------------------------------------------------- //
// Print types

void
print(std::ostream& os, const type_seq& ts)
{
  for (auto iter = ts.begin(); iter != ts.end(); ++iter) {
    print(os, *iter);
    if (std::next(iter) != ts.end())
      os << ',' << ' ';
  }
}

void
print_unit_type(std::ostream& os, const unit_type* t)
{
  os << "unit";
}

void
print_bool_type(std::ostream& os, const bool_type* t)
{
  os << "bool";
}

void
print_int_type(std::ostream& os, const int_type* t)
{
  os << "int";
}

void
print_ph_type(std::ostream& os, const ph_type* t)
{
  os << "?";
}

void
print_arrow_type(std::ostream& os, const arrow_type* t)
{
  enclose enc(os, parens);
  print(os, t->parm);
  os << ' ';
  print(os, t->ret);
}

void
print_fn_type(std::ostream& os, const fn_type* t)
{
  enclose enc(os, parens);
  {
    enclose parms(os, parens);
    print(os, t->parms);
  }
  os << " -> ";
  print(os, t->ret);
}

void
print_tuple_type(std::ostream& os, const tuple_type* t)
{
  enclose enc(os, braces);
  print(os, t->elems);
}

void
print(std::ostream& os, const field* f)
{
  os << f->id;
  os << " : ";
  print(os, f->ty);
}

void
print(std::ostream& os, const field_seq& fs)
{
  for (auto iter = fs.begin(); iter != fs.end(); ++iter) {
    print(os, *iter);
    if (std::next(iter) != fs.end())
      os << ',' << ' ';
  }
}

void
print_record_type(std::ostream& os, const record_type* t)
{
  enclose(os, braces);
  print(os, t->fields);
}

void
print_variant_type(std::ostream& os, const variant_type* t)
{
  enclose(os, angles);
  print(os, t->fields);
}

void
print_ptr_type(std::ostream& os, const ptr_type* t)
{
  enclose enc(os, parens);
  print(os, t->elem);
  os << '*';
}

void
print_ref_type(std::ostream& os, const ref_type* t)
{
  enclose enc(os, parens);
  print(os, t->obj);
  os << '&';
}

void
print(std::ostream& os, const type* t)
{
  switch (t->kind) {
#define def_type(T) \
  case tk_ ## T: \
    return print_ ## T ##_type(os, (const T ## _type*)t);
#include "type.def"
  } 
}

std::ostream& 
operator<<(std::ostream& os, const expr* e)
{
  print(os, e);
  return os;
}

std::ostream& 
operator<<(std::ostream& os, const type* t)
{
  print(os, t);
  return os;
}
